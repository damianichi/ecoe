/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

    //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
    //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
    //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝
    'GET /': { action: 'view-homepage-or-redirect' },
    'GET /welcome/:unused?': { action: 'dashboard/view-welcome' },


    'GET /faq': { action: 'view-faq' },
    'GET /legal/terms': { action: 'legal/view-terms' },
    'GET /legal/privacy': { action: 'legal/view-privacy' },
    'GET /contact': { action: 'view-contact' },

    'GET /signup': { action: 'entrance/view-signup' },
    'GET /email/confirm': { action: 'entrance/confirm-email' },
    'GET /email/confirmed': { action: 'entrance/view-confirmed-email' },

    'GET /login': { action: 'entrance/view-login' },
    'GET /password/forgot': { action: 'entrance/view-forgot-password' },
    'GET /password/new': { action: 'entrance/view-new-password' },

    'GET /account': { action: 'account/view-account-overview' },
    'GET /account/password': { action: 'account/view-edit-password' },
    'GET /account/profile': { action: 'account/view-edit-profile' },


    //'GET /ecoe/': 'EcoeController.obtenerECOE',

    //manuales
    'GET /alumno': { action: 'view-alumno' },
    'GET /newEcoe': { action: 'ecoe/view-new-ecoe' },
    //'GET /ecoe': { action: 'ecoe/view-ecoe' },
    'GET /ecoes/:virtualPageSlug?': { action: 'ecoe/view-ecoe-2' },
    'GET /estaciones/:id': { action: 'estacion/view-estacion' },
    'GET /signup-docente': { action: 'view-signup-docente' },
    'GET /docente/': { action: 'estacion/view-docente-estaciones' },
    'GET /estacion/contenido/:idEstacion': { action: 'view-contenido' },
    'GET /estacion/contenido/items/:idEstacion': { action: 'view-elementosrubrica' },
    'GET /signup-estudiante': { action: 'view-signup-estudiante' },
    'GET /preparacion': { action: 'view-preparacion' },
    'GET /api/getHora': 'GethoraController.obtenerHora',
    'GET /api/v1/getstatus/:idECOE': { action: 'getstatus' },
    'GET /:idEstacion/preevaluacion/': { action: 'view-preevaluacion' },
    'GET /ejecucionecoe/:idEstacion/': { action: 'view-ejecucionecoe' },
    'GET /evaluacion/:idestacion/:idparticipante': { action: 'view-evaluacion' },
    'GET /monitor/:idEcoe': { action: 'view-monitor' },
    'GET /resultados/:idEcoe': { action: 'view-resultados' },
    'GET /alumno/resultado/:idParticipacion': { action: 'view-resultadoalumno' },
    //'GET /alumno/resultado/:idParticipacion': { action: 'view-resultadoalumno' },



    //'/estaciones/:id': 'estacion/EstacionController.mostrar', //vieja .. de controlador no usar
    'GET /api/v1/getdocentes': { action: 'getdocentes' },
    'GET /api/v1/get-img/:id/descargarimagen': { action: 'get-img' },
    'POST /api/v1/ecoe/registroecoe': { action: 'ecoe/registroecoe' },
    'POST /api/v1/ecoe/borrarecoe': { action: 'ecoe/borrarecoe' },
    'POST /api/v1/signup-docente': { action: 'signupdocente' }, //registro docente post (action)
    'POST /api/v1/obtenerestacion': { action: 'obtenerestacion' }, //para enviar id estacion
    'POST /api/v1/borrarestacion': { action: 'borrarestacion' },
    'POST /api/v1/newcontenido': { action: 'newcontenido' },
    'POST /api/v1/registroelemento': { action: 'registroelemento' },
    'POST /api/v1/borrarelemento': { action: 'borrarelemento' },
    'POST /api/v1/aprobarestacion': { action: 'aprobarestacion' },
    'POST /api/v1/ingresarobservacion': { action: 'ingresarobservacion' },
    'POST /api/v1/modificarelemento': { action: 'modificarelemento' },
    'POST /api/v1/ecoeestadocompletado': { action: 'ecoeestadocompletado' },
    'POST /api/v1/modificarnombreecoe': { action: 'modificarnombreecoe' },
    'POST /api/v1/signupestudiante': { action: 'signupestudiante' },
    'POST /api/v1/modificacionparticipantes': { action: 'modificacionparticipantes' },
    'POST /api/v1/newestacion': { action: 'newestacion' },
    'POST /api/v1/cambiar-a-ejecucion': { action: 'cambiaraejecucion' },
    'POST /api/v1/vincularprofesorestacion': { action: 'vincularprofesorestacion' },
    'POST /api/v1/confirmacionprofesor': { action: 'confirmacionprofesor' },
    'POST /api/v1/enviarevaluacion': { action: 'enviarevaluacion' },
    'POST /api/v1/finalizarexamen': { action: 'finalizarexamen' },
    'POST /api/v1/changevisualizacion': { action: 'changevisualizacion' },
    'POST /api/v1/changeallvisualizacion': { action: 'changeallvisualizacion' },
    'POST /api/v1/pausarecoe': { action: 'pausarecoe' },

    //SOCKET comunicacion

    'POST /on-connect': 'SocketconexionController.onConnect',
    'POST /send-message': 'SocketconexionController.sendMessage', //metodo prueba
    'POST /iniciar': 'SocketconexionController.iniciarECOE',

    'POST /api/v1/iniciar-circuito': { action: 'iniciar-circuito' },

    
    'POST /api/v1/modificarcontenidoprofesor': { action: 'modificarcontenidoprofesor' },








    //  ╔╦╗╦╔═╗╔═╗  ╦═╗╔═╗╔╦╗╦╦═╗╔═╗╔═╗╔╦╗╔═╗   ┬   ╔╦╗╔═╗╦ ╦╔╗╔╦  ╔═╗╔═╗╔╦╗╔═╗
    //  ║║║║╚═╗║    ╠╦╝║╣  ║║║╠╦╝║╣ ║   ║ ╚═╗  ┌┼─   ║║║ ║║║║║║║║  ║ ║╠═╣ ║║╚═╗
    //  ╩ ╩╩╚═╝╚═╝  ╩╚═╚═╝═╩╝╩╩╚═╚═╝╚═╝ ╩ ╚═╝  └┘   ═╩╝╚═╝╚╩╝╝╚╝╩═╝╚═╝╩ ╩═╩╝╚═╝
    '/terms': '/legal/terms',
    '/logout': '/api/v1/account/logout',


    //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
    //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
    //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝
    // …


    //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
    //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
    //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝
    // Note that, in this app, these API endpoints may be accessed using the `Cloud.*()` methods
    // from the Parasails library, or by using those method names as the `action` in <ajax-form>.
    '/api/v1/account/logout': { action: 'account/logout' },
    'PUT   /api/v1/account/update-password': { action: 'account/update-password' },
    'PUT   /api/v1/account/update-profile': { action: 'account/update-profile' },
    'PUT   /api/v1/account/update-billing-card': { action: 'account/update-billing-card' },
    'PUT   /api/v1/entrance/login': { action: 'entrance/login' },
    'PUT    /api/v1/updatevisualizacionresult': { action: 'updatevisualizacionresult' },








    'POST  /api/v1/entrance/signup': { action: 'entrance/signup' },
    'POST  /api/v1/entrance/send-password-recovery-email': { action: 'entrance/send-password-recovery-email' },
    'POST  /api/v1/entrance/update-password-and-login': { action: 'entrance/update-password-and-login' },
    'POST  /api/v1/deliver-contact-form-message': { action: 'deliver-contact-form-message' },

};