parasails.registerPage('monitor', {
    //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
    //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
    //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
    data: {

        ecoe: [],




        //-------- variables TIEMPO---
        horaactual: null, //hora actual del servidor
        diferencialhora: null,
        tiempoctual: null,
        tiempoTOTAL: null,
        tiempoTermino: 0,
        intervalos: [],
        tiempoTotalRestante: 0,



        //------------end TIME----

        ModalFinalizar: false,
        ModalPausa: false,
        modalCancelarPrepa: false,
        //-------submit

        formData: {},
        formErrors: { /* … */ },
        syncing: false,
        cloudError: '',
        cloudSuccess: false,
        //…
    },

    //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
    //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
    //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
    beforeMount: function() {
        // Attach any initial data from the server.
        _.extend(this, SAILS_LOCALS);
        console.log(this.ecoe)
        this.GenerarIntervalos();
        this.diferencialhora = this.horaactual - new Date().getTime(); // calcula la diferencia de hora del servidor comparada con la del browser . asi despues restar el diferencial y guiar cronometro a partir del relj del navegador
        var ms = (new Date().getTime() + this.diferencialhora) - this.ecoe.tiempoInicio + this.ecoe.tiempoAcumulado;
        console.log(this.ecoe.participantesECOE.length)


        if (this.ecoe.estaciones.length > this.ecoe.participantesECOE.length) {
            this.tiempoTermino = (this.ecoe.estaciones.length * (this.ecoe.minutos * 60 + this.ecoe.entretiempo) + this.ecoe.entretiempo) * 1000;
        } else {
            this.tiempoTermino = (this.ecoe.participantesECOE.length * (this.ecoe.minutos * 60 + this.ecoe.entretiempo) + this.ecoe.entretiempo) * 1000;
        }
        //this.tiempoTermino = (31000 * (this.ecoe.minutos * 60 + this.ecoe.entretiempo) + this.ecoe.entretiempo) * 1000;
        if (this.tiempoTotalRestante >= 0) {
            this.tiempoTotalRestante = this.formatDuration((this.tiempoTermino - ms) + 1000);
        } else {
            this.tiempoTotalRestante = this.formatDuration(0);
        }

        console.log(this.formatDuration(this.tiempoTermino))
        console.log(this.formatDuration(ms))
        this.tiempoTOTAL = this.formatDuration(ms);
        this.tiempoctual = this.cicloactual(ms);
    },
    mounted: async function() {
        //…
        this.obtenerhora(); //actualizar hora una vez montado

        var ms = 0;

        while (true) {
            ms = (new Date().getTime() + this.diferencialhora) - (this.ecoe.tiempoInicio + this.ecoe.tiempoAcumulado); //revisa reloj del navegador le resta el diferencial de hora respecto a la hora del servidor y luego obtiene el avance haciendo la resta
            this.tiempoTOTAL = this.formatDuration(ms);
            this.tiempoctual = this.cicloactual(ms);
            if (Number(this.tiempoTermino - ms) > 0) {
                this.tiempoTotalRestante = this.formatDuration((this.tiempoTermino - ms) + 1000);
            } else {

                this.tiempoTotalRestante = this.formatDuration(0);
            }

            console.log(this.tiempoTotalRestante)
            await this.sleep(1000); //duerme 1 seg ... no quitar o sino browser explota

        }
    },

    //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
    //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
    methods: {



        GenerarIntervalos: function() {
            if (this.intervalos.length != 0) {
                this.intervalos = [];
            }
            //var Num_staciones = this.ecoe.estaciones.length; //obtiene cantidad de estaciones
            //var Num_staciones = 31000; //obtiene cantidad de estaciones
            console.log(this.ecoe)
            var repeticion = 0;
            if (this.ecoe.estaciones.length > this.ecoe.participantesECOE.length) {
                console.log(this.ecoe.estaciones.length)
                console.log("---------------")
                console.log(this.ecoe.participantesECOE.length)
                repeticion = this.ecoe.estaciones.length;
            } else {
                repeticion = this.ecoe.participantesECOE.length;
            }

            var minutos = this.ecoe.minutos // minutos por estacion
                //var minutos = 1.5 // minutos por estacion SOLO DEVELOP

            var minutosMs = minutos * 60 * 1000; //minutos por estacion pasado a ms
            var entretiempo = this.ecoe.entretiempo * 1000; //45 segundos a ms ... entretiempo entre estacion
            // var entretiempo = 20 * 1000 //45 segundos a ms ... SOLO DEVELOP
            var cont = 0;
            //var cont2 = minutosMs
            var cont2 = entretiempo;
            for (var i = 0; i < repeticion; i++) {
                this.intervalos.push(new Object({ inicio: cont, termino: cont2, estado: false }));
                cont = cont2 + 1;
                cont2 = cont2 + minutosMs;
                this.intervalos.push(new Object({ inicio: cont, termino: cont2, estado: true }));
                cont = cont2 + 1;
                cont2 = cont2 + entretiempo;
            }
            this.intervalos.push(new Object({ inicio: cont, termino: cont2, estado: false }));
            console.log(this.intervalos)
        },

        cicloactual: function(tiempoactual) {
            var tiempoCiclo = 0;
            for (var j = 0; j < this.intervalos.length; j++) {
                if (tiempoactual >= this.intervalos[j].inicio && tiempoactual <= this.intervalos[j].termino) { // verifica ciclo en intervalos
                    h = tiempoactual - this.intervalos[j].inicio; // calcula el tiempo actual
                    tiempoCiclo = (this.intervalos[j].termino - (this.intervalos[j].inicio - 1)) //calcula el tiempo del ciclo
                    h = (tiempoCiclo - h) + 1000; //hace la diferencia para ver el tiempo en forma de temporizador
                    this.estadociclo = this.intervalos[j].estado; //define el estado en que se encuentra el ingreso de datos
                    return this.formatDuration(h);
                }
            }
            this.estadociclo = false;
            return 0;
        },


        obtenerhora: async function() {
            fetch("/api/getHora").then(response => response.json()).then((data) => { //ob
                this.horaactual = data.horaactual;
            });
        },
        clickbotonCancelarPrep: function(input) {
            this.formData = {};
            this.formData.idECOE = input;
            this.modalCancelarPrepa = true;
        },
        //---------------------cambiar estado -----
        submittedFormEstadoAprobado: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            window.location.reload();
        },

        handleParsingFormEstadoAprobado: function() {
            // limpia errores viejos
            this.formErrors = {};

            var argins = this.formData;

            if (this.ecoe.participantesECOE.length === 0) {
                this.formErrors.participantes = true;

            }


            if (Object.keys(this.formErrors).length > 0) {
                return;
            }

            return argins;
        },


        sleep: function(ms) { // espera un segundo

            return new Promise(resolve => setTimeout(resolve, ms));
        },
        formatDuration: function(ms) { //formatea tipo hora ---> 00:00:00
            var duration = moment.duration(ms);
            if (duration.asHours() > 1) { return Math.floor(duration.asHours()) + moment.utc(duration.asMilliseconds()).format(":mm:ss"); } else { return moment.utc(duration.asMilliseconds()).format("mm:ss"); }
        },
        //…

        //-----------------Finalizar Examen
        submittedFormFinalizar: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            window.location = '/resultados/' + this.ecoe.id;
        },

        handleParsingFormFinalizar: function() {
            // limpia errores viejos
            this.formErrors = {};
            this.formData.idEcoe = this.ecoe.id
            var argins = this.formData;


            if (Object.keys(this.formErrors).length > 0) {
                return;
            }

            return argins;
        },
        submittedFormPausar: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            window.location = '/estaciones/' + this.ecoe.id;
        },

        handleParsingFormPausar: function() {
            // limpia errores viejos
            this.formErrors = {};
            this.formData.idECOE = this.ecoe.id;
            this.formData.tipo = true;
            var argins = this.formData;
            if (Object.keys(this.formErrors).length > 0) {
                return;
            }
            return argins;
        },






    }
    //…
});