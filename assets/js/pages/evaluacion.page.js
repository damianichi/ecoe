parasails.registerPage('evaluacion', {
    //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
    //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
    //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
    data: {
        tiempoctual: null,
        horaactual: null,
        diferencialhora: null,
        ecoe: [],
        modalContenido: false,
        URLL: null,
        intervalos: [],
        estadociclo: false,
        elementos: [],
        porcentajes: [],
        formData: {} /* … */ ,
        ModalSave: false,
        estacion: [],
        alumno: [],
        textosplit: '',

        //---------errores-----    
        ModalError: false,
        msgError: '',



        //-----------------submit

        formErrors: { /* … */ },
        syncing: false,
        // Server error state
        cloudError: '',
        // Success state when form has been submitted
        cloudSuccess: false,
    },

    //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
    //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
    //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
    beforeMount: function() {
        // Attach any initial data from the server.
        _.extend(this, SAILS_LOCALS);

        for (var u = 0; u < this.elementos.length; u++) {
            this.porcentajes.push(new Object({ id: this.elementos[u].id, item: this.elementos[u].item, porcentajeObtenido: 0 }));
        }
        if (this.estacion.contenido.length != 0) {
            this.textosplit = this.estacion.contenido[0].texto.split('<br />');
        }
        this.GenerarIntervalos();
        console.log(this.intervalos)
        this.diferencialhora = this.horaactual - new Date().getTime();


        var ms = (new Date().getTime() + this.diferencialhora) - (this.ecoe.tiempoInicio + this.ecoe.tiempoAcumulado);
        //    this.tiempoctual = this.formatDuration(ms);

        this.tiempoctual = this.cicloactual(ms);
        console.log(this.ecoe)
        this.URLLL = this.geturl();




    },
    mounted: async function() {

        this.obtenerhora();
        var ms = 0;
        while (true) {
            ms = (new Date().getTime() + this.diferencialhora) - (this.ecoe.tiempoInicio + this.ecoe.tiempoAcumulado); //revisa reloj del navegador le resta el diferencial de hora respecto a la hora del servidor y luego obtiene el avance haciendo la resta
            this.tiempoctual = this.cicloactual(ms);
            // this.tiempoctual = this.formatDuration(ms);
            await this.sleep(1000); //duerme 1 seg ... no quitar o sino browser explota
            if (!this.estadociclo) {
                this.ModalSave = true;
            }
            if (this.ModalError === true) {
                break;
            }
        }
    },

    methods: {


        GenerarIntervalos: function() {
            if (this.intervalos.length != 0) {
                this.intervalos = [];
            }
            //var Num_staciones = this.ecoe.estaciones.length; //obtiene cantidad de estaciones

            //var Num_staciones = 31000; //obtiene cantidad de estaciones  DEVELOP
            var repeticion = 0;
            if (this.ecoe.estaciones.length > this.ecoe.participantesECOE.length) {
                console.log(this.ecoe.estaciones.length)
                console.log("---------------")
                console.log(this.ecoe.participantesECOE.length)
                repeticion = this.ecoe.estaciones.length;
            } else {
                repeticion = this.ecoe.participantesECOE.length;
            }
            var minutos = this.ecoe.minutos // minutos por estacion
                //var minutos = 1.5 // minutos por estacion DEVELOP
            var minutosMs = minutos * 60 * 1000; //minutos por estacion pasado a ms
            var entretiempo = this.ecoe.entretiempo * 1000 //45 segundos a ms ... entretiempo entre estacion
                //var entretiempo = 20 * 1000 //45 segundos a ms ... entretiempo entre estacion
            var cont = 0;
            //var cont2 = minutosMs
            var cont2 = entretiempo
            for (var i = 0; i < repeticion; i++) {
                this.intervalos.push(new Object({ inicio: cont, termino: cont2, estado: false }));
                cont = cont2 + 1
                cont2 = cont2 + minutosMs
                this.intervalos.push(new Object({ inicio: cont, termino: cont2, estado: true }));
                cont = cont2 + 1
                cont2 = cont2 + entretiempo
            }
            this.intervalos.push(new Object({ inicio: cont, termino: cont2, estado: false }));
            console.log(this.intervalos)
        },

        cicloactual: function(tiempoactual) {
            var tiempoCiclo = 0;
            for (var j = 0; j < this.intervalos.length; j++) {
                if (tiempoactual >= this.intervalos[j].inicio && tiempoactual <= this.intervalos[j].termino) { // verifica ciclo en intervalos
                    h = tiempoactual - this.intervalos[j].inicio; // calcula el tiempo actual
                    tiempoCiclo = (this.intervalos[j].termino - (this.intervalos[j].inicio - 1)) //calcula el tiempo del ciclo
                    h = (tiempoCiclo - h) + 1000; //hace la diferencia para ver el tiempo en forma de temporizador
                    this.estadociclo = this.intervalos[j].estado; //define el estado en que se encuentra el ingreso de datos
                    return this.formatDuration(h);
                }
            }
            this.estadociclo = false;
            return 0;
        },

        obtenerhora: async function() {
            fetch("/api/getHora").then(response => response.json()).then((data) => { //ob
                this.horaactual = data.horaactual;
            });
        },

        sleep: function(ms) { // espera un segundo

            return new Promise(resolve => setTimeout(resolve, ms));
        },
        formatDuration: function(ms) { //formatea tipo hora ---> 00:00:00
            var duration = moment.duration(ms);
            if (duration.asHours() > 1) { return Math.floor(duration.asHours()) + moment.utc(duration.asMilliseconds()).format(":mm:ss"); } else { return moment.utc(duration.asMilliseconds()).format("mm:ss"); }
        },

        geturl: function() {
            var base_url = window.location.origin;
            var host = window.location.host;
            var pathArray = window.location.pathname.split('/');
            return base_url + "/";
        },

        //-------------------------------ENVIO EVALUACION---------

        submittedFormEvaluacion: async function(response) {

            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            if (response.statusCode && response.statusCode === 400) {
                this.msgError = "Ya existe una evaluación";
                this.ModalError = true;
                await this.sleep(5000);
                window.location = '/ejecucionecoe/' + this.estacion.id;

            } else {
                if (response.statusCode && response.statusCode === 408) {
                    this.msgError = "Usted se encuentra fuera del periodo de evaluacion";
                    this.ModalError = true;

                    await this.sleep(5000);
                    console.log("entre aca2")
                    window.location = '/ejecucionecoe/' + this.estacion.id; //redirigir a participantes

                } else {
                    window.location = '/ejecucionecoe/' + this.estacion.id; //redirigir a participantes
                }
            }
        },
        redirectLISTA: function() {
            window.location = '/ejecucionecoe/' + this.estacion.id
        },

        handleParsingFormEvaluacion: function() {
            // limpia errores viejos
            this.formErrors = {};
            for (var l = 0; l < this.porcentajes.length; l++) {
                this.porcentajes[l].porcentajeObtenido = Number(this.porcentajes[l].porcentajeObtenido);
            }
            //console.log(this.porcentajes)
            this.formData.porcentajes = this.porcentajes;
            this.formData.idEstacion = this.estacion.id;
            this.formData.alumnoID = this.alumno.id; //id como participante
            var argins = this.formData;

            if (Object.keys(this.formErrors).length > 0) {
                return;
            }

            return argins;
        },

    }
});