parasails.registerPage('estaciones', {
    //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
    //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
    //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
    data: {
        virtualPageSlug: false,
        virtualPageVincular: false,
        virtualPagemodificar: false,
        ChangestateECOE: false,
        modalAddEstudiante: false,
        modalAddEstacion: false,
        modalCancelarPrepa: false,
        InicioEJEC: false,
        modaledit: false,

        estacionesReact: [],

        formValidationNewEstacion: {},
        formValidation: {},
        todayDate: '',


        iniciarEcoe: false,
        CambiarEstadoEstacion: true,

        alertaingreso: false,


        ECOE: [],


        //arreglos para trabajar modal de participantes
        estudiantes: [], //llega desde el action
        IngresoParticipante: [],
        eliminacionParticipante: [],
        busqueda: '',
        EstadoPreparacion: false,
        //------------------END-----------------------
        virtualPages: true,
        formData: {},
        docentes: [],
        profesorTemp: '',
        formErrors: { /* … */ },
        syncing: false,
        // Server error state
        cloudError: '',
        // Success state when form has been submitted
        cloudSuccess: false,
        dateEcoe: ''
    },
    //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
    //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
    //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
    beforeMount: function() {
        // Attach any initial data from the server.
        _.extend(this, SAILS_LOCALS);
        this.dateEcoe = moment(new Date(this.ECOE.fecha)).format('DD-MM-YYYY')
        this.todayDate = moment(new Date()).format('YYYY-MM-DD');
        for (var i = 0; i < this.ECOE.estaciones.length; i++) {
            if (this.ECOE.estaciones[i].estado_preparacion != "revisado")
                this.CambiarEstadoEstacion = false;
        }

        if (this.EstadoPreparacion === true) {
            this.estacionesReact = this.ECOE.estaciones; //intento de estaciones reactivas 
            for (var i = 0; i < this.estacionesReact.length; i++) {
                if (this.estacionesReact[i].estado_ejecucion == "no-preparado") {
                    console.log(this.estacionesReact[i].estado_ejecucion)
                    this.iniciarEcoe = false;
                }
            }

        }

    },
    mounted: async function() {
        var cont = 0;

        if (this.EstadoPreparacion === true) {

            io.socket.post('/on-connect', { idEcoe: this.ECOE.id, _csrf: SAILS_LOCALS._csrf }, function(data, data2) { //conectamos socket con servidor
                console.log(data);
            })

            while (true) {
                cont = this.estacionesReact.length;

                //fetch("/api/getHora").then(response => response.json()).then((data) => { this.fecha = data.hora; });




                //console.log("estaciones actualizando");

                fetch("/api/v1/getstatus/" + this.ECOE.id).then(response => response.json()).then((data) => { this.estacionesReact = data; }); //actualiza el estado de las estaciones    

                //cambiar con socket!!!!!
                this.iniciarEcoe = true;
                for (var i = 0; i < this.estacionesReact.length; i++) {
                    if (this.estacionesReact[i].estado_ejecucion === "no-preparado") {
                        console.log(this.estacionesReact[i].estado_ejecucion)
                        this.iniciarEcoe = false;
                        cont--;
                    }
                }
                console.log("----")
                if (cont == this.estacionesReact.length) {
                    //ya que estan todos preparados, deja de consultar!
                    break;
                }
                await this.sleep(4000);

            }



        }

    },

    methods: {



        iniciarECOE: function() { //boton para cambiar estado y mandar mensaje por socket a los docentes
            io.socket.post('/api/v1/iniciar-circuito', { idECOE: this.ECOE.id, _csrf: SAILS_LOCALS._csrf }, function(data, data2) {
                console.log(data);

                window.location = '/monitor/' + data.idEcoe
            })
        },

        botonRefrescar: function() {
            window.location.reload();
        },

        goEstacion: function(idEstacion) {
            window.location = '/estacion/contenido/' + idEstacion;

        },

        quitarEstudianteLista: function(idPart) {
            
            if (idPart) {
                console.log("hola2")
                var index = _.findIndex(this.IngresoParticipante, { id: idPart });
                var removedItem = this.IngresoParticipante.splice(index, 1);
                console.log("item eliminado: " + removedItem)
            }
        },
        borrarAlumnoParticipante: function(alumno) {

            console.log("entre a borrar");
            console.log(alumno)
            if (_.find(this.ECOE.participantesECOE, { 'id': alumno.id })) {
                console.log("borrando")
                this.eliminacionParticipante.unshift({
                    rut: alumno.rut,
                    nombre: alumno.nombreParticipante,
                    id: alumno.estudianteId
                });
                var index = _.findIndex(this.ECOE.participantesECOE, { id: alumno.id });
                var removedItem = this.ECOE.participantesECOE.splice(index, 1);
            }

            console.log(this.eliminacionParticipante);





        },

        Agregarnuevo: function(alumno) {
            this.alertaingreso = false;

            console.log("CLIIIICK");
            if (!_.find(this.IngresoParticipante, { 'id': alumno.id })) {
                if (!_.find(this.ECOE.participantesECOE, { 'estudianteId': alumno.id })) {
                    this.IngresoParticipante.unshift({
                        rut: alumno.rut,
                        nombre: alumno.nombre,
                        id: alumno.id
                    });
                } else {
                    this.alertaingreso = true;

                    console.log("ingresado1");
                }
            } else {
                console.log("ingresado2");
                this.alertaingreso = true;
            }

        },



        //---------------------cambiar estado -----
        submittedFormEstadoAprobado: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            window.location.reload();
        },

        handleParsingFormEstadoAprobado: function() {
            // limpia errores viejos
            this.formErrors = {};

            var argins = this.formData;

            if (this.ECOE.participantesECOE.length === 0) {
                this.formErrors.participantes = true;

            }


            if (Object.keys(this.formErrors).length > 0) {
                return;
            }

            return argins;
        },




        //------botones modales.------
        clickbotonAgregarAlumno: function(input) {
            this.formData = {};
            this.formData.idECOE = input;
            this.modalAddEstudiante = true;
        },



        clickbotonCambioEstadoEcoe: function(input) {
            this.formData = {};
            this.formData.idECOE = input;
            this.ChangestateECOE = true;
        },
        clickbotonInicioEjec: function(input) {
            this.formData = {};
            this.formData.idECOE = input;
            this.InicioEJEC = true;
        },
        clickbotonNuevaEstacion: function(input) {
            this.formValidationNewEstacion = {};
            this.formData = {};
            this.formData.estaciones_para_aprobar = Number(this.ECOE.estaciones_para_aprobar);
            this.idECOE = input;
            console.log(this.ECOE.estaciones_para_aprobar);
            this.formData.idECOE = this.idECOE;
            this.modalAddEstacion = true;
        },


        clickbotonEliminar: function(input, ecoeID) {
            this.formValidationNewEstacion = {};
            this.formData = {};
            this.formData.estaciones_para_aprobar = Number(this.ECOE.estaciones_para_aprobar);
            this.idBorrar = input;
            console.log(this.idBorrar);
            this.formData.idBorrar = this.idBorrar;
            this.formData.idECOE = ecoeID;
            this.virtualPageSlug = true;
            console.log("formdatra", this.FormData)
        },
        clickbotonVincularProfesor: function(input) {
            this.idVincular = input;
            console.log("idEstacion" + this.idVincular);
            this.formData.idEstacionVincular = this.idVincular;
            fetch("/api/v1/getdocentes").then(response => response.json()).then((data) => { this.docentes = data; }); //trae los datos desde la url .. con get 
            this.virtualPageVincular = true;
        },
        clickbotonModificarProfesor: function(input1, input2) { //input2= id estacion, input1=idProfesor de estacion
            this.idVincular = input2;
            console.log("input 2  :" + input2);
            console.log("input 1  :" + input1);
            this.formData.idEstacionVincular = this.idVincular;
            fetch("/api/v1/getdocentes").then(response => response.json()).then((data) => {
                this.docentes = data,
                    this.profesorTemp = _.find(this.docentes, { 'id': input1 }).nombre; //guardamos el nombre del profesor

            }); //trae los datos desde la url .. con get 

            // console.log(profesorTemp);
            this.virtualPagemodificar = true;
        },
        cerrarBotonEliminar: function() { // hay q modificarlos ... no se estan utilizando .. en estaciones.ejs
            console.log("cancelar");
            this.formValidationNewEstacion = {};
            this.virtualPageSlug = false;
            this.formData.idBorrar = null;

            this.idBorrar = '';
        },
        cerrarBotonVincularProfesor: function() {
            console.log("cancelar");

            this.virtualPageSlug = false;
            this.formData.idBorrar = null;

            this.idBorrar = '';
        },
        cerrarBotonModificarProfesor: function() {
            console.log("cancelar");

            this.virtualPageSlug = false;
            this.formData.idBorrar = null;

            this.idBorrar = '';
        },




        //-----------------ingresa docente---------------

        submittedFormgetdocentes: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            // window.location = response;

            window.location.reload(); //recarga la pagina xD esto puede mejorar cargando en un principio un arreglo a vue a traves de get desde el controlador ... 
        },

        handleParsinggetdocentes: function() {
            // limpia errores viejos
            this.formErrors = {};

            var argins = this.formData;
            console.log(argins);


            if (Object.keys(this.formErrors).length > 0) {
                return;
            }

            return argins;
        },
        //-----------------Modificar Participantes---------------

        submittedFormParticipantes: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            window.location.reload();
        },

        handleParsingFormParticipantes: function() {
            // limpia errores viejos
            this.formErrors = {};
            let formComplete = {
                ingreso: _.cloneDeep(this.IngresoParticipante),
                eliminacion: _.cloneDeep(this.eliminacionParticipante),
                idECOE: this.ECOE.id
            };
            var argins = formComplete;

            console.log(argins);


            if (Object.keys(this.formErrors).length > 0) {
                return;
            }

            return argins;
        },
        //-----------------nueva estacion---------------

        submittedFormNewEstacion: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            window.location.reload();
        },


        handleParsingFormNewEstacion: function() {
            this.formErrors = {};
            this.formValidationNewEstacion = {}
            var argins = this.formData;
            if (!argins.NombreEstación) {
                this.formErrors.NombreEstación = true;
            }
            if (!argins.estaciones_para_aprobar) {
                this.formErrors.estaciones_para_aprobar = true;
            } else {
                if (!(this.formData.estaciones_para_aprobar > 0 && this.formData.estaciones_para_aprobar <= (Number(this.ECOE.estaciones.length) + 1))) {
                    this.formValidationNewEstacion.estaciones_para_aprobar = true;
                    this.formErrors.error = true;
                } else {
                    this.formValidationNewEstacion.estaciones_para_aprobar = false;
                }
            }
            if (Object.keys(this.formErrors).length > 0) {
                return;
            }
            return argins;
        },




        //-----------------eliminar---------------
        submittedFormEliminar: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            window.location = response;

            //window.location = '/';
        },

        handleParsingFormEliminar: function() {
            // limpia errores viejos
            this.formErrors = {};
            this.formValidationNewEstacion = {};

            var argins = this.formData;

            if (!argins.estaciones_para_aprobar) {
                this.formErrors.estaciones_para_aprobar = true;
            } else {
                if (!(this.formData.estaciones_para_aprobar >= 0 && this.formData.estaciones_para_aprobar <= (Number(this.ECOE.estaciones.length) - Number(1)))) {
                    this.formValidationNewEstacion.estaciones_para_aprobar = true;
                    this.formErrors.error = true;
                } else {
                    if (Number(this.formData.estaciones_para_aprobar) === 0 & Number(this.ECOE.estaciones.length > 1)) {
                        this.formValidationNewEstacion.estaciones_para_aprobar = true;
                        this.formErrors.error = true;
                    } else { this.formValidationNewEstacion.estaciones_para_aprobar = false; }

                }
            }


            if (Object.keys(this.formErrors).length > 0) {
                return;
            }

            return argins;
        },



        //------- cancelar preparacion-----

        clickbotonCancelarPrep: function(input) {
            this.formData = {};
            this.formData.idECOE = input;
            this.modalCancelarPrepa = true;
        },





        sleep: function(ms) { // espera un segundo

            return new Promise(resolve => setTimeout(resolve, ms));
        },




        //----- modificar EXAMEN-----
        clickbotonEditar: function() {
            this.formEdit = {};
            this.formEdit.id = this.ECOE.id;
            this.formEdit.nombre = this.ECOE.nombreEcoe;
            this.formEdit.fecha = moment(this.ECOE.fecha).format('YYYY-MM-DD')
            this.formEdit.minutos = this.ECOE.minutos;
            this.formEdit.entretiempo = this.ECOE.entretiempo;
            this.formEdit.estaciones_para_aprobar = this.ECOE.estaciones_para_aprobar;
            //console.log(this.input.id);
            //this.formData.idEditar = this.input.id;
            this.modaledit = true;
        },


        submittedFormEditarECOE: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            window.location.reload();

            //window.location = '/';
        },

        handleParsingFormEditarECOE: function() {
            // limpia errores viejos
            this.formErrors = {};
            this.formValidation = {}

            var argins = this.formEdit;
            console.log(this.formEdit)
            if (!argins.nombre) {
                console.log("cayo")
                this.formErrors.nombre = true;
            }
            if (!argins.fecha) {
                this.formErrors.fecha = true;
            }
            if (!argins.minutos) {
                this.formErrors.minutos = true;
            }
            if (!argins.entretiempo) {
                this.formErrors.entretiempo = true;
            }
            if (!argins.estaciones_para_aprobar) {
                this.formErrors.estaciones_para_aprobar = true;
            } else {

                if (argins.estaciones_para_aprobar) {
                    console.log(argins.estaciones_para_aprobar + "----" + this.ECOE.estaciones.length)
                    if (!(argins.estaciones_para_aprobar > 0 && argins.estaciones_para_aprobar <= this.ECOE.estaciones.length)) {
                        this.formValidation.estaciones_para_aprobar = true;
                        this.formErrors.error = true;
                    } else {
                        this.formValidation.estaciones_para_aprobar = false;
                    }
                } else {
                    this.formValidation.estaciones_para_aprobar = false;
                    this.formErrors.estaciones_para_aprobar = true;

                }
            }


            if (Object.keys(this.formErrors).length > 0) {
                return;
            }

            return argins;
        },


    }
});