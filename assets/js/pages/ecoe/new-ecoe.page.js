parasails.registerPage('new-ecoe', {
    //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
    //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
    //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
    data: {
        // Form data
        formData: { /* … */ },

        // For tracking client-side validation errors in our form.
        // > Has property set to `true` for each invalid property in `formData`.
        formErrors: { /* … */ },
        formValidation: { /* … */ },

        todayDate: '',

        // Syncing / loading state
        syncing: false,

        // Server error state
        cloudError: '',

        // Success state when form has been submitted
        cloudSuccess: false,
    },

    //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
    //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
    //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
    beforeMount: function() {
        // Attach any initial data from the server.
        _.extend(this, SAILS_LOCALS);
        this.todayDate = moment(new Date()).format('YYYY-MM-DD');


    },
    mounted: async function() {
        //…
    },

    //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
    //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
    methods: {

        submittedForm: async function(response) {
            console.log(response);
            if (response.APIstatus == "200") {
                // If email confirmation is enabled, show the success message.
                this.cloudSuccess = true;
            } else {
                // Error message will display in the 
                this.syncing = true;
            }
            //window.location = '/ecoes';

            window.location = response;
        },

        handleParsingForm: function() {
            // Clear out any pre-existing error messages.
            this.formErrors = {};

            //var argins = this.formData;
            var argins = this.formData;

            if (!argins.nombreEcoe) {
                this.formErrors.nombreEcoe = true;
            } else {
                if (argins.nombreEcoe.length > 50) {
                    this.formValidation.nombreEcoe = true;
                } else {
                    this.formValidation.nombreEcoe = false;
                };

            }
            if (!argins.nEstaciones) {
                this.formErrors.nEstaciones = true;
            }
            if (!argins.fecha) {
                this.formErrors.fecha = true;
            }
            if (!argins.minutos) {
                this.formErrors.minutos = true;
            }
            if (!argins.entretiempo) {
                this.formErrors.entretiempo = true;
            }

            if (!argins.estaciones_para_aprobar) {
                this.formErrors.estaciones_para_aprobar = true;
            } else {

                if (argins.estaciones_para_aprobar) {
                    if (!(this.formData.estaciones_para_aprobar > 0 && this.formData.estaciones_para_aprobar <= this.formData.nEstaciones)) {
                        this.formValidation.estaciones_para_aprobar = true;
                        this.formErrors.error = true;
                    } else {
                        this.formValidation.estaciones_para_aprobar = false;
                    }
                } else {
                    this.formValidation.estaciones_para_aprobar = false;
                    this.formErrors.estaciones_para_aprobar = true;

                }
            }



            /* Validate full name:
            if(!argins.fullName) {
              this.formErrors.fullName = true;
            }*/



            // If there were any issues, they've already now been communicated to the user,
            // so simply return undefined.  (This signifies that the submission should be
            // cancelled.)
            if (Object.keys(this.formErrors).length > 0) {
                return;
            }

            return argins;
        },

    }
});