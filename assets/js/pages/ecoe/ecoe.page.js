parasails.registerPage('ecoe', {
    //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
    //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
    //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝


    data: {
        virtualPageSlug: false,
        //… 


        virtualPages: true,
        ecoes: [],
        formData: {},
        // For tracking client-side validation errors in our form.
        // > Has property set to `true` for each invalid property in `formData`.
        formErrors: { /* … */ },

        // Syncing / loading state
        syncing: false,

        // Server error state
        cloudError: '',

        // Success state when form has been submitted
        cloudSuccess: false,
    },


    //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
    //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
    //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
    beforeMount: function() {
        // Attach any initial data from the server.
        _.extend(this, SAILS_LOCALS);
        this.ecoes = _.sortBy(this.ecoes, ['createdAt'], ['asc']);
    },
    mounted: async function() {
        //…
    },

    //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
    //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
    methods: {


        clickbotonEliminar: function(input) {
            this.idBorrar = input;
            console.log(this.idBorrar);
            this.formData.idBorrar = this.idBorrar;
            this.virtualPageSlug = true;
        },



        cerrarBotonEliminar: function() {
            console.log("cancelar");

            this.virtualPageSlug = false;
            this.formData.idBorrar = null;

            this.idBorrar = '';
        },




        //------eliminar----
        submittedForm: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            window.location = '/ecoes';

            //window.location = '/';
        },

        handleParsingForm: function() {
            // limpia errores viejos
            this.formErrors = {};

            var argins = this.formData;


            if (Object.keys(this.formErrors).length > 0) {
                return;
            }

            return argins;
        },

    }
});