parasails.registerPage('preparacion', {
    //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
    //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
    //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
    data: {

        fecha: null,
        f: null,
        variable: "hola",
        dataSocket: '',
        nuevo: '',
        //…
    },

    //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
    //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
    //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
    beforeMount: function() {
        // Attach any initial data from the server.
        var a = "texto guardado";
        //this.fecha = fetch("/api/getHora").then(response => response.json()).then((data) => { this.fecha = data.hora; });
        console.log("iniciando configuracion de socket");
        io.socket.on('connect', function() {
            console.log("connected");
            io.socket.post('/on-connect', { idEcoe: '2', _csrf: SAILS_LOCALS._csrf }, function(data, data2) {
                console.log(data);
            })
        })


        _.extend(this, SAILS_LOCALS);
    },
    mounted: async function() {

        /*while (true) {

            await this.sleep(1000);

            fetch("/api/getHora").then(response => response.json()).then((data) => { this.fecha = data.hora; });

        }*/
        io.socket.on('chat', function(data) {
            SAILS_LOCALS.dataSocket = data;

        })

        while (true) {
            await this.sleep(1000);
            /* io.socket.get('/api/getHora', function(data, data2) {
                     SAILS_LOCALS.fecha = data.hora;
                     //this.fechaDOS = data.hora;
                 })*/
            //this.fecha = window.fechaDOS;
            if (SAILS_LOCALS.fecha)
                this.fecha = SAILS_LOCALS.fecha;
            this.dataSocket = SAILS_LOCALS.dataSocket;
        }
        console.log("termino!")





    },

    //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
    //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝

    methods: {


        sleep: function(ms) { // espera un segundo

            return new Promise(resolve => setTimeout(resolve, ms));
        },
        send: function(data) {
                console.log("la data : " + data)
                io.socket.post('/send-message', { dato: data, _csrf: SAILS_LOCALS._csrf }, function(data, data2) {
                    console.log(data);
                })


            }
            //…
    }
});