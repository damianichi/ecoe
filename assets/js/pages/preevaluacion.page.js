parasails.registerPage('preevaluacion', {
    //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
    //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
    //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
    data: {

        ViewBotonAdd: true,
        formVista: false,
        ModalPreparado: false,
        dataECOE: [],
        idestacion: null,



        formErrors: { /* … */ },
        syncing: false,
        cloudError: '',
        URLLL: ''


        //…
    },

    beforeMount: function() {
        _.extend(this, SAILS_LOCALS);
        console.log(SAILS_LOCALS._csrf) // codigo para enviar post
        this.URLLL = this.geturl();
        console.log("iniciando configuracion de socket");
        if (this.dataECOE.estacion.contenido.length != 0) {
            this.textosplit = this.dataECOE.estacion.contenido[0].texto.split('<br />');
        }
        io.socket.on('connect', function() {
            console.log("connected");
        })




    },
    mounted: async function() {
        console.log(this.dataECOE)

    },
    methods: {



        geturl: function() {
            var base_url = window.location.origin;
            var host = window.location.host;
            var pathArray = window.location.pathname.split('/');
            return base_url + "/";
        },

        botonmodalPrep: function(idestacion) {
            this.formData = {};
            this.formData.idestacion = idestacion;
            this.ModalPreparado = true;
        },

        submittedFormEstadoPrep: async function(response) {
            console.log(response);
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            //window.location = response;
            if (!response.msg) {
                window.location = '/ejecucionecoe/' + this.dataECOE.estacion.id;
            } else {
                window.location = '/docente';

            }
        },

        handleParsingFormEstadoPrep: function(idestacion) {
            // limpia errores viejos
            this.formErrors = {};
            var argins = this.formData;
            if (Object.keys(this.formErrors).length > 0) {
                return;
            }

            return argins;
        },




        //…
    }
});