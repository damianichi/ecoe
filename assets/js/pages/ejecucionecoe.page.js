parasails.registerPage('ejecucionecoe', {
    //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
    //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
    //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
    data: {

        estacion: [],
        esperandoInicio: true,
        ecoe: [],
        tiempoctual: null,
        tiempoTOTAL: null,
        horaactual: null, //hora actual del servidor
        diferencialhora: null,
        intervalos: [],
        estadociclo: false,
        //…
    },

    //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
    //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
    //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
    beforeMount: function() {
        _.extend(this, SAILS_LOCALS);
        var URLactual = window.location;
        if (this.estacion.id != URLactual.pathname.split("/")[2]) {
            console.log("algo anda ma .. cuidao");
            location.reload();
        }
        console.log();
        //-------------------
        //horaactual viene al llamado de la pagina entregando la hora del servidor
        this.GenerarIntervalos();
        this.diferencialhora = this.horaactual - new Date().getTime(); // calcula la diferencia de hora del servidor comparada con la del browser . asi despues restar el diferencial y guiar cronometro a partir del relj del navegador
        //console.log(this.diferencialhora);
        if (this.esperandoInicio) {
            io.socket.post('/on-connect', { idEcoe: this.ecoe.id, _csrf: SAILS_LOCALS._csrf }, function(data, data2) { //conectamos socket con servidor
                console.log("conexion con socket : " + data);
            })
        }
        var ms = (new Date().getTime() + this.diferencialhora) - (this.ecoe.tiempoInicio + this.ecoe.tiempoAcumulado);
        this.tiempoTOTAL = this.formatDuration(ms);
        this.tiempoctual = this.cicloactual(ms);
    },
    mounted: async function() {

        console.log(this.ecoe)


        this.obtenerhora(); //actualizar hora una vez montado

        var ms = 0;
        if (this.esperandoInicio) { //si estamos esperando que se inicie el ecoe .. quedamos a  la escucha con el socket
            io.socket.on('estado-de-inicio', function(data) { //esta a la escucha de un evento cambio de estado
                // SAILS_LOCALS.dataSocket = data;   //Prueba .. IGNORAR
                location.reload(); //recarga a los escucha para q consulten denuevo
            })
        }
        while (true) {
            ms = (new Date().getTime() + this.diferencialhora) - (this.ecoe.tiempoInicio + this.ecoe.tiempoAcumulado); //revisa reloj del navegador le resta el diferencial de hora respecto a la hora del servidor y luego obtiene el avance haciendo la resta
            this.tiempoTOTAL = this.formatDuration(ms);
            this.tiempoctual = this.cicloactual(ms);
            console.log(this.tiempoTotalRestante)
            await this.sleep(1000); //duerme 1 seg ... no quitar o sino browser explota

        }
    },

    //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
    //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
    methods: {



        GenerarIntervalos: function() {
            if (this.intervalos.length != 0) {
                this.intervalos = [];
            }
            //var Num_staciones = this.ecoe.estaciones.length; //obtiene cantidad de estaciones
            var repeticion = 0;
            if (this.ecoe.estaciones.length > this.ecoe.participantesECOE.length) {
                repeticion = this.ecoe.estaciones.length;
            } else {
                repeticion = this.ecoe.participantesECOE.length;
            }
            //var Num_staciones = 31000; //obtiene cantidad de estaciones
            var minutos = this.ecoe.minutos // minutos por estacion
                //var minutos = 1.5 // minutos por estacion SOLO DEVELOP
            var minutosMs = minutos * 60 * 1000; //minutos por estacion pasado a ms
            var entretiempo = this.ecoe.entretiempo * 1000 //45 segundos a ms ... entretiempo entre estacion
                //var entretiempo = 20 * 1000 //45 segundos a ms ... SOLO DEVELOP
            var cont = 0;
            //var cont2 = minutosMs
            var cont2 = entretiempo
            for (var i = 0; i < repeticion; i++) {
                this.intervalos.push(new Object({ inicio: cont, termino: cont2, estado: false }));
                cont = cont2 + 1
                cont2 = cont2 + minutosMs
                this.intervalos.push(new Object({ inicio: cont, termino: cont2, estado: true }));
                cont = cont2 + 1
                cont2 = cont2 + entretiempo
            }
            this.intervalos.push(new Object({ inicio: cont, termino: cont2, estado: false }));
            console.log(this.intervalos)
        },

        cicloactual: function(tiempoactual) {
            var tiempoCiclo = 0;
            for (var j = 0; j < this.intervalos.length; j++) {
                if (tiempoactual >= this.intervalos[j].inicio && tiempoactual <= this.intervalos[j].termino) { // verifica ciclo en intervalos
                    h = tiempoactual - this.intervalos[j].inicio; // calcula el tiempo actual
                    tiempoCiclo = (this.intervalos[j].termino - (this.intervalos[j].inicio - 1)) //calcula el tiempo del ciclo
                    h = (tiempoCiclo - h) + 1000; //hace la diferencia para ver el tiempo en forma de temporizador
                    this.estadociclo = this.intervalos[j].estado; //define el estado en que se encuentra el ingreso de datos
                    return this.formatDuration(h);
                }
            }
            this.estadociclo = false;
            return 0;
        },

        irEvaluacion: function(idParticipante, idEstacion) {
            // console.log("idParticipante: " + idParticipante)
            //console.log("idEstacion: " + idEstacion)
            window.location = "/evaluacion/" + idEstacion + "/" + idParticipante; //redirige al ingreso de la evaluacion
        },
        obtenerhora: async function() {
            fetch("/api/getHora").then(response => response.json()).then((data) => { //ob
                this.horaactual = data.horaactual;
            });
        },

        sleep: function(ms) { // espera un segundo

            return new Promise(resolve => setTimeout(resolve, ms));
        },
        formatDuration: function(ms) { //formatea tipo hora ---> 00:00:00
                var duration = moment.duration(ms);
                if (duration.asHours() > 1) { return Math.floor(duration.asHours()) + moment.utc(duration.asMilliseconds()).format(":mm:ss"); } else { return moment.utc(duration.asMilliseconds()).format("mm:ss"); }
            }
            //…
    }
});