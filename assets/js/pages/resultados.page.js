parasails.registerPage('resultados', {
    //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
    //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
    //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
    data: {
        submited: false,
        submitedAll: false,
        ecoe: [],
        formUpdate: {},
        participacion: {},
        modalupdate: false,
        modalLiberarNotas: false,
        modalOcultarNotas: false,

        syncing: false,
        // Form data
        formData: { /* … */ },

        formErrors: { /* … */ },

        cloudError: '',

        cloudSuccess: false,
    },

    //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
    //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
    //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
    beforeMount: function() {
        // Attach any initial data from the server.
        _.extend(this, SAILS_LOCALS);
        console.log(this.ecoe);
    },
    mounted: async function() {
        //…
    },

    //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
    //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
    methods: {

        CambiarEstado: function(id, index) {
            // limpia errores viejos
            this.submited = true;
            this.formErrors = {};
            console.log(this.ecoe.participantesECOE[index])


            fetch("/api/v1/changevisualizacion", {
                method: 'POST',
                body: JSON.stringify({ idParticipante: id, _csrf: SAILS_LOCALS._csrf }),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(response => response.json()).then((data) => {

                window.location.reload();

            }); //actualiza el estado de las estaciones

        },
        CambiarAllEstado: function(typeAction) {
            this.submitedAll = true;
            fetch("/api/v1/changeallvisualizacion", {
                method: 'POST',
                body: JSON.stringify({ idEcoe: this.ecoe.id, typeAction: typeAction, _csrf: SAILS_LOCALS._csrf }),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(response => response.json()).then((data) => {

                window.location.reload();

            }); //actualiza el estado de las estaciones

        },





        clickbotonUpdateVisualizacion: function(input) {
            this.participacion = input;
            console.log(this.participacion)
            this.modalupdate = true;
        },
        submittedFormUpdate: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            //window.location.reload();
            //window.location = '/';
        },

        handleParsingFormUpdate: function() {
            // limpia errores viejos
            this.formErrors = {};
            var argins = this.formUpdate;
            if (Object.keys(this.formErrors).length > 0) {
                return;
            }

            return argins;
        },
    }
});