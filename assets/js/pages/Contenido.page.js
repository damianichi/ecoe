parasails.registerPage('contenido', {
    //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
    //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
    //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
    data: {

        ViewBotonAdd: true,
        formVista: false,
        modaledit: false,
        todayDate: '',

        dataECOE: {},
        formValidation: {},
        formEdit: {
            image: undefined,
            image2: undefined,
            image3: undefined,
            texto: '',
            estacionProp: null,
        },

        uploadFormData: {
            image: undefined,
            image2: undefined,
            image3: undefined,
            texto: '',
            estacionProp: null,
        },

        formErrors: { /* … */ },
        syncing: false,
        cloudError: '',

        textosplit: null

        //…
    },

    //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
    //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
    //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
    beforeMount: function() {
        // Attach any initial data from the server.
        _.extend(this, SAILS_LOCALS);
        console.log(this.dataECOE)
        if (this.dataECOE.estacion.contenido.length != 0) {
            this.textosplit = this.dataECOE.estacion.contenido[0].texto.split('<br />');
        }

    },
    mounted: async function() {
        //…
    },

    //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
    //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
    methods: {


        formularioContenido: function() { // hay q modificarlos ... no se estan utilizando .. en estaciones.ejs
            this.ViewBotonAdd = false;
            this.formVista = true;
        },
        deleteImageEdit: function(imageNum) {
            console.log("eliminando: "+ imageNum )
            switch(imageNum){
                case 1: 
                    this.formEdit.image= undefined;
                    console.log("")
                    this.methodThatForcesUpdate();
                    break;
                case 2:
                    this.formEdit.image2= undefined;
                    this.methodThatForcesUpdate();
                    break;
                case 3:
                    this.formEdit.image3= undefined;
                    this.methodThatForcesUpdate();
                    break;
            }
             // Notice we have to use a $ here
            // ...
        },
        methodThatForcesUpdate: function() {
            // ...
            this.$forceUpdate();  // Notice we have to use a $ here
            // ...
          },
        changeFileInput: async function(files) {
            if (files.length !== 1 && !this.uploadFormData.image) {
                throw new Error('Consistency violation: `changeFileInput` was somehow called with an empty array of files, or with more than one file in the array!  This should never happen unless there is already an uploaded file tracked.');
            }
            var selectedFile = files[0];
            if (!selectedFile && this.uploadFormData.image) {
                return;
            }
            var FileSize = selectedFile.size / 1024 / 1024; // in MB
            if (selectedFile.type != 'image/jpeg') {
                alert('Formato incorrecto, este debe ser jpg');
                event.srcElement.value = null;
                return;
            }
            if (FileSize > 1) {
                alert('Imagen excede el tamaño de 1 MB');
                event.srcElement.value = null;
                return;
            }

            var reader = new FileReader();
            reader.onload = (event) => {
                this.uploadFormData.image = event.target.result;
                delete reader.onload;
            };
            this.formErrors.image = false;
            reader.readAsDataURL(selectedFile);

        },
        changeFileInput2: async function(files) {
            if (files.length !== 1 && !this.uploadFormData.image2) {
                throw new Error('Consistency violation: `changeFileInput` was somehow called with an empty array of files, or with more than one file in the array!  This should never happen unless there is already an uploaded file tracked.');
            }
            var selectedFile = files[0];
            if (!selectedFile && this.uploadFormData.image2) {
                return;
            }
            var FileSize = selectedFile.size / 1024 / 1024; // in MB
            if (selectedFile.type != 'image/jpeg') {
                alert('Formato incorrecto, este debe ser jpg');
                event.srcElement.value = null;
                return;
            }
            if (FileSize > 1) {
                alert('Imagen excede el tamaño de 1 MB');
                event.srcElement.value = null;
                return;
            }
            var reader = new FileReader();
            reader.onload = (event) => {
                this.uploadFormData.image2 = event.target.result;
                delete reader.onload;
            };
            this.formErrors.image2 = false;
            reader.readAsDataURL(selectedFile);

        },
        changeFileInput3: async function(files) {
            if (files.length !== 1 && !this.uploadFormData.image3) {
                throw new Error('Consistency violation: `changeFileInput` was somehow called with an empty array of files, or with more than one file in the array!  This should never happen unless there is already an uploaded file tracked.');
            }
            var selectedFile = files[0];
            if (!selectedFile && this.uploadFormData.image3) {
                return;
            }
            var FileSize = selectedFile.size / 1024 / 1024; // in MB
            if (selectedFile.type != 'image/jpeg') {
                alert('Formato incorrecto, este debe ser jpg');
                event.srcElement.value = null;
                return;
            }
            if (FileSize > 1) {
                alert('Imagen excede el tamaño de 1 MB');
                event.srcElement.value = null;
                return;
            }
            var reader = new FileReader();
            reader.onload = (event) => {
                this.uploadFormData.image3 = event.target.result;
                delete reader.onload;
            };
            this.formErrors.image3 = false;
            reader.readAsDataURL(selectedFile);

        },
        changeFileInputEdit: async function(files) {
            if (files.length !== 1 && !this.formEdit.image) {
                throw new Error('Consistency violation: `changeFileInput` was somehow called with an empty array of files, or with more than one file in the array!  This should never happen unless there is already an uploaded file tracked.');
            }
            var selectedFile = files[0];
            if (!selectedFile && this.formEdit.image) {
                return;
            }
            var FileSize = selectedFile.size / 1024 / 1024; // in MB
            if (selectedFile.type != 'image/jpeg') {
                alert('Formato incorrecto, este debe ser jpg');
                event.srcElement.value = null;
                return;
            }
            if (FileSize > 1) {
                alert('Imagen excede el tamaño de 1 MB');
                event.srcElement.value = null;
                return;
            }
            var reader = new FileReader();
            reader.onload = (event) => {
                this.formEdit.image = event.target.result;
                delete reader.onload;
                this.methodThatForcesUpdate();
            };
            this.formErrors.image = false;
            reader.readAsDataURL(selectedFile);
          

        },
        changeFileInputEdit2: async function(files) {
            if (files.length !== 1 && !this.formEdit.image2) {
                throw new Error('Consistency violation: `changeFileInput` was somehow called with an empty array of files, or with more than one file in the array!  This should never happen unless there is already an uploaded file tracked.');
            }
            var selectedFile = files[0];
            if (!selectedFile && this.formEdit.image2) {
                return;
            }
            var FileSize = selectedFile.size / 1024 / 1024; // in MB
            if (selectedFile.type != 'image/jpeg') {
                alert('Formato incorrecto, este debe ser jpg');
                event.srcElement.value = null;
                return;
            }
            if (FileSize > 1) {
                alert('Imagen excede el tamaño de 1 MB');
                event.srcElement.value = null;
                return;
            }
            var reader = new FileReader();
            reader.onload = (event) => {
                this.formEdit.image2 = event.target.result;
                delete reader.onload;
                this.methodThatForcesUpdate();
            };
            this.formErrors.image2 = false;
            reader.readAsDataURL(selectedFile);
           

        },
        changeFileInputEdit3: function(files) {

            if (files.length !== 1 && !this.formEdit.image3) {
                throw new Error('Consistency violation: `changeFileInput` was somehow called with an empty array of files, or with more than one file in the array!  This should never happen unless there is already an uploaded file tracked.');
            }
            var selectedFile = files[0];
            if (!selectedFile && this.formEdit.image3) {
                return;
            }
            var FileSize = selectedFile.size / 1024 / 1024; // in MB
            if (selectedFile.type != 'image/jpeg') {
                alert('Formato incorrecto, este debe ser jpg');
                event.srcElement.value = null;
                return;
            }
            if (FileSize > 1) {
                alert('Imagen excede el tamaño de 1 MB');
                event.srcElement.value = null;
                return;
            }
            var reader = new FileReader();
            reader.onload = (event) => {
                this.formEdit.image3 = event.target.result;
                delete reader.onload;
                this.methodThatForcesUpdate();
            };
            console.log("se cambio la wea")
            this.formErrors.image3 = false;
            reader.readAsDataURL(selectedFile);
            

        },


        handleParsingUploadContenidoForm: function() {
            this.formErrors = {};
            this.uploadFormData.texto = this.uploadFormData.texto.replace(/[\n\r]/g, "<br />");
            var argins = this.uploadFormData;
            if (!argins.texto) {
                this.formErrors.texto = true;
            }
            if (!argins.estacionProp) {
                this.formErrors.estacionProp = true;
            }
            if ((!argins.image) &&(!argins.image2) && (!argins.image3)) {
                this.formErrors.estacionProp = true;
            }

            if (Object.keys(this.formErrors).length > 0) {
                return;
            }
            return _.omit(argins);
        },

        submittedFormContenido: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }

            window.location.reload(); //recarga la pagina xD
        },


        clickbotonEditar: function() {
            this.formEdit = {};
            this.formEdit.texto = this.dataECOE.estacion.contenido[0].texto.replace(/<br ?\/?>/gi, "\n");
            //this.formEdit.texto = this.dataECOE.estacion.contenido[0].texto;
            //document.getElementById("textoDescription").value = document.getElementById("textoDescription").innerHTML.replace(/&nbsp;/, '').replace(/<br ?\/?>/, '\n');
            if (this.dataECOE.estacion.contenido[0].image) {
                this.formEdit.image = this.dataECOE.estacion.contenido[0].image;
            }
            if (this.dataECOE.estacion.contenido[0].image2) {
                this.formEdit.image2 = this.dataECOE.estacion.contenido[0].image2;
            }
            if (this.dataECOE.estacion.contenido[0].image3) {
                this.formEdit.image3 = this.dataECOE.estacion.contenido[0].image3;
            }

            /*this.formEdit.id = this.ECOE.id;
            
            this.formEdit.fecha = moment(this.ECOE.fecha).format('YYYY-MM-DD')
            this.formEdit.minutos = this.ECOE.minutos;
            this.formEdit.entretiempo = this.ECOE.entretiempo;
            this.formEdit.estaciones_para_aprobar = this.ECOE.estaciones_para_aprobar;*/
            //console.log(this.input.id);
            //this.formData.idEditar = this.input.id;


            this.modaledit = true;
        },

        submittedFormEditarECOE: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            window.location.reload();

            //window.location = '/';
        },

        handleParsingFormEditarECOE: function() {
            // limpia errores viejos
            this.formErrors = {};
            this.formValidation = {}
            this.formEdit.id= this.dataECOE.estacion.contenido[0].id;
            var argins = this.formEdit;
            console.log(this.formEdit)
            if (!argins.texto) {
              
                this.formErrors.texto = true;
            }
            if (!argins.id) {
              
                this.formErrors.id = true;
            }
            if ((!argins.image) && (!argins.image2) && (!argins.image3)) {
                this.formErrors.images = true;
            }
        

            if (Object.keys(this.formErrors).length > 0) {
                return;
            }

            return argins;
        },


        eliminarImagenNew: async function(Numimage) {
            switch (Numimage) {
                case 1:
                    this.uploadFormData.image = undefined;
                    break;
                case 2:
                    this.uploadFormData.image2 = undefined;
                    break;
                case 3:
                    this.uploadFormData.image3 = undefined;
                    break;
            }
        },
        eliminarImagenNewModify: async function(Numimage) {
            switch (Numimage) {
                case 1:
                    this.formEdit.image = undefined;
                    break;
                case 2:
                    this.formEdit.image2 = undefined;
                    break;
                case 3:
                    this.formEdit.image3 = undefined;
                    break;
            }
        }



    }
});