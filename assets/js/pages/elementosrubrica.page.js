parasails.registerPage('elementosrubrica', {
    //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
    //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
    //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
    data: {

        ecoe: [],
        estacion: [],
        elementos: [],
        Formelementos: [],

        totalPorciento: 0,
        totalPorcientoEDIT: 0,

        maximo: false,
        maximoEdit: false,



        formErrors: {

        }, //guardar errores 
        formEstado: {

        },

        modalBorrar: false,
        modalEditar: false,
        modoEdicion: false, //borrar
        modalObs: false,
        virtualPageSlugNew: false,
        syncing: false,
        formEditar: {

        },
        formDataObs: {

        },
        formData: {

        },
        formBorrar: {

        },
        cloudError: '',
        cloudSuccess: false,



        //…
    },






    //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
    //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
    //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
    beforeMount: function() {
        // Attach any initial data from the server.
        _.extend(this, SAILS_LOCALS);
        for (var i = 0; i < this.elementos.length; i++) {
            this.totalPorciento += Number(this.elementos[i].porcentajeLogrado);
            this.Formelementos = this.elementos;

        }
        console.log(this.totalPorciento);
        if (this.estacion.observacion) {
            this.formDataObs.observacion = this.estacion.observacion;
        }
    },
    mounted: async function() {
        //…
    },


    methods: {
        //…


        clickbotonObservacion: function() {

            this.modalObs = true;
        },



        modoedicion: function() {
            this.modoEdicion = true;
        },
        modoVista: function() {
            this.modoEdicion = false;
        },

        //----------------ESTADO----------
        submittedFormEstado: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            window.location.reload();
            //window.location = '/';
        },

        handleParsingEstado: function() {
            // limpia errores viejos
            this.formErrors = {};
            console.log(this.estacion.id);
            this.formEstado.idEstacion = Number(this.estacion.id);
            var argins = this.formEstado;


            if (Object.keys(this.formErrors).length > 0) {
                return;
            }

            return argins;
        },


        //-----------------END-----------


        //----------------Observacion----------
        submittedFormObs: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            window.location.reload();
            //window.location = '/';
        },

        handleParsingObs: function() {
            // limpia errores viejos
            this.formErrors = {};
            this.formDataObs.idEstacion = Number(this.estacion.id);

            var argins = this.formDataObs;
            if (!argins.observacion) {
                this.formErrors.observacion = true;
            }


            if (Object.keys(this.formErrors).length > 0) {
                return;
            }

            return argins;
        },


        //-----------------END-----------

        //-------------------editar---------

        clickbotonEditar: function(input) {
            this.formErrors = {};
            this.formEditar = {}; //vaciamos el form cada vez que editemos .. asi limpiar la modificacion
            console.log(input);
            /*this.idEditar = input;
            console.log(this.idEditar);
            this.formEditar.idEditar = this.idEditar;*/
            this.totalPorcientoEDIT = this.totalPorciento - input.porcentajeLogrado;
            if (input) {
                this.formEditar.idEditar = input.id;
                this.formEditar.item = input.item;
                this.formEditar.nota = input.nota;
                this.formEditar.porcentajeLogrado = input.porcentajeLogrado;
                this.formEditar.porcentajeMedio = input.porcentajeMedio;
            }
            this.modalEditar = true;
        },

        submittedFormEditar: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            this.virtualPageSlugNew = false; //no sirve luego q recarga ;D
            window.location.reload();
            //window.location = '/';
        },

        handleParsingFormEditar: function() {
            // limpia errores viejos
            this.maximoEdit = false;
            this.formErrors = {};

            var argins = this.formEditar;

            if (!argins.item) {
                this.formErrors.item = true;
            }
            if (!argins.nota) {
                this.formErrors.nota = true;
            }
            var pregunta = Number(argins.porcentajeLogrado) + Number(this.totalPorcientoEDIT);
            if (Number(pregunta) > 100) {

                console.log("se paso con un total de" + pregunta);
                this.maximoEdit = true;
            }


            if (!argins.porcentajeLogrado) {
                this.formErrors.porcentajeLogrado = true;
            } else {
                if (Number(argins.porcentajeLogrado) < 1 || Number(argins.porcentajeLogrado) > 100) { //validacion porcentaje

                    this.formErrors.porcentajeLogrado = true;
                }
            }
            if (!argins.porcentajeMedio) {
                console.log("entre2");


                this.formErrors.porcentajeMedio = true;

            } else {

                if (Number(argins.porcentajeMedio) < 0 || Number(argins.porcentajeMedio) >= Number(argins.porcentajeLogrado)) { //validacion porcentaje
                    console.log("medio: " + argins.porcentajeMedio + ">= logrado" + argins.porcentajeLogrado)


                    this.formErrors.porcentajeMedio = true;
                }
            }

            if (Object.keys(this.formErrors).length > 0 || this.maximoEdit) {
                return;
            }
            return argins;
        },











        //-------------------eliminar---------


        //-------------------eliminar---------
        clickbotonEliminar: function(input) {
            this.idBorrar = input;
            console.log(this.idBorrar);
            this.formBorrar.idBorrar = this.idBorrar;
            this.modalBorrar = true;
        },
        submittedFormEliminar: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            window.location.reload();
            //window.location = '/';
        },

        handleParsingFormEliminar: function() {
            // limpia errores viejos
            this.formErrors = {};

            var argins = this.formBorrar;


            if (Object.keys(this.formErrors).length > 0) {
                return;
            }

            return argins;
        },

        //-----------------------end ---------------

        //-----------------------NEW ---------------
        modonew: function() {
            this.formErrors = {};
            this.virtualPageSlugNew = true;
        },


        submittedForm: async function(response) {
            if (response.APIstatus == "200") {
                this.cloudSuccess = true;
            } else {
                this.syncing = true;
            }
            this.virtualPageSlugNew = false;
            this.goto('/estacion/contenido/items/' + this.estacion.id);

            //window.location = '/';
        },

        handleParsingForm: function() {
            // limpia errores viejos
            this.maximo = false;
            this.formErrors = {};

            this.formData.idContenido = this.estacion.contenido[0].id;


            var argins = this.formData;

            if (!argins.item) {
                this.formErrors.item = true;
            }
            if (!argins.nota) {
                this.formErrors.nota = true;
            }
            var pregunta = Number(argins.porcentajeLogrado) + Number(this.totalPorciento);
            if (Number(pregunta) > 100) {

                console.log("se paso con un total de" + pregunta);
                this.maximo = true;
            }


            if (!argins.porcentajeLogrado) {
                this.formErrors.porcentajeLogrado = true;
            } else {
                if (Number(argins.porcentajeLogrado) < 1 || Number(argins.porcentajeLogrado) > 100) { //validacion porcentaje

                    this.formErrors.porcentajeLogrado = true;
                }
            }
            if (!argins.porcentajeMedio) {
                this.formErrors.porcentajeMedio = true;

            } else {

                if (Number(argins.porcentajeMedio) < 0 || Number(argins.porcentajeMedio) >= Number(argins.porcentajeLogrado)) { //validacion porcentaje
                    console.log("medio: " + argins.porcentajeMedio + ">= logrado" + argins.porcentajeLogrado)


                    this.formErrors.porcentajeMedio = true;
                }
            }

            if (Object.keys(this.formErrors).length > 0 || this.maximo) {
                return;
            }
            return argins;
        },

        //-----------------------end ---------------

        goEcoe: function() {
            this.goto('/estaciones/' + this.ecoe.id);

        }


    }
});