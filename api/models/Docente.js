/**
 * Docente.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
    tableName: 'Docentes',
    attributes: {

        idUser: {
            type: 'number',
            unique: true,

        },
        rut: {
            type: 'string',

            columnName: 'rut_docente'

        },
        nombre: {
            type: 'string',
            columnName: 'nombre_docente'
        },
        correo: {
            type: 'string',
            unique: true,
            columnName: 'correo_docente'
        },
        estaciones: { // atributo "virtual" para asociar diferentes ecoes(uno a mucho)
            collection: 'estacion', // tipo de modelo a colleccionar
            via: 'docenteid' // nombre atributo el cual tendra relacion en modelo de Ecoe.js

        }
    },

};