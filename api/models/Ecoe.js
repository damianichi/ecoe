/**
 * Ecoe.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
    tableName: 'Ecoes',
    attributes: {


        //idEcoe: { //el id lo genera automaticamente sails con nombre "id", para acceder .. "ecoe.id"
        //   type: 'number',
        //  unique: true
        //},
        nombreEcoe: {
            type: 'string'
        },
        nEstaciones: {
            type: 'number'
        },
        fecha: {
            type: 'ref',
            columnType: "datetime"
        },
        estadoEcoe: {
            type: 'string'
        },

        tiempoInicio: { //tiempo en ms de cuando se inició el EXAMEN
            type: 'number',
            columnType: 'bigint',
            defaultsTo: 0,
        },
        tiempoInicioPausa: { //tiempo en ms de cuando se inició el EXAMEN
            type: 'number',
            columnType: 'bigint',
            defaultsTo: 0,
        },
        tiempoAcumulado: { //tiempo en ms de cuando se inició el EXAMEN
            type: 'number',
            columnType: 'bigint',
            defaultsTo: 0,
        },

        coordinadorEcoe: {
            model: 'coordinador' //nombre modelo que conecta la relacion
        },

        estaciones: { // atributo "virtual" para asociar diferentes estaciones(uno a mucho) ===> arreglo de estaciones ;D
            collection: 'estacion', // tipo de modelo a colleccionar
            via: 'ECOEid' // nombre atributo el cual tendra relacion en modelo de estacion.js   ej= this.id=estacion.ECOEid

        },
        participantesECOE: {
            collection: 'participantes', // tipo de modelo a colleccionar
            via: 'ECOEid' // nombre atributo el cual tendra relacion en modelo de estacion.js   ej= this.id=estacion.ECOEid

        },
        minutos: { //tiempo que tendra cada estacion para ser evaluada (EN MINUTOS)
            type: 'number',
            defaultsTo: 7
        },
        estaciones_para_aprobar: { //cantidad de estaciones para poder aprobar
            type: 'number',
            defaultsTo: 1
        },
        entretiempo: { //entretiempo entre estaciones EN SEGUNDOS.
            type: 'number',
            defaultsTo: 45
        },



    },

};