/**
 * Estacion.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
    tableName: 'Estaciones',
    attributes: {
        nombre: {
            type: 'string' //nombre de la estacion
        },
        estado_ejecucion: { //preparacion si no ... cuando comience circuito
            type: 'string'

        },
        estado_preparacion: { //estacion de revision 
            type: 'string'
        },
        observacion: { //observacion dada en caso de que en la revision se encuentren incoherencias.
            type: 'string'
        },
        docenteid: {
            model: 'docente'
        },
        ECOEid: {
            model: 'ecoe'
        },
        contenido: {
            collection: 'Contenido',
            via: 'estacionProp'
        }

        /* contenido: { // atributo "virtual" para asociar diferentes estaciones(uno a mucho) ===> arreglo de estaciones ;D
             collection: 'contenido', // tipo de modelo a colleccionar
             via: 'idcontenido' // nombre atributo el cual tendra relacion en modelo de estacion.js   ej= this.id=estacion.ECOEid

         },
         */







    },

};