/**
 * Item_evaluado.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {

        porcentaje_obtenido: {
            type: 'number'
        },
        Pregunta_asociada: {
            type: 'string'
        },
        Evaluacion_ID: {
            model: 'Estacion_evaluada'
        },


    },

};