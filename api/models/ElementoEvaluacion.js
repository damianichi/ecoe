/**
 * ElementoEvaluacion.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
    tableName: 'Elementos_rubrica',

    attributes: {

        item: {
            type: 'string'
        },
        porcentajeLogrado: {
            type: 'number'
        },
        porcentajeMedio: {
            type: 'number'
        },
        nota: {
            type: 'string'
        },

        idContenido: {
            model: 'contenido'
        }




    },

};