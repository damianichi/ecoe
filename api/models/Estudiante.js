/**
 * Estudiante.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    idUser:{
      type: 'number',
      unique: true,
    },
    nombre:{
      type:'string'
    },
    rut:{
      type:'number',
      
    },
    correo:{
      type: 'string',
      unique: true,
      columnName: 'correo_estudiante'
    },

    participaciones: { 
      collection: 'participantes',
      via: 'estudianteId' // nombre atributo el cual tendra relacion en modelo 
}

  },

};

