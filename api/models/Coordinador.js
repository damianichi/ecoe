/**
 * Coordinador.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    tableName: 'Coordinadores',

    attributes: {



        idUser: {
            type: 'number',
            unique: true,

        },
        rut: {
            type: 'string',

            columnName: 'rut_cordinador'

        },
        nombre: {
            type: 'string',
            columnName: 'nombre_cordinador'
        },
        correo: {
            type: 'string',
            unique: true,
            columnName: 'correo_cordinador'
        },

        ecoes: { // atributo "virtual" para asociar diferentes ecoes(uno a mucho)
            collection: 'ecoe', // tipo de modelo a colleccionar
            via: 'coordinadorEcoe' // nombre atributo el cual tendra relacion en modelo de Ecoe.js

        }

    },

};