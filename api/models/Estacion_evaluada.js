/**
 * Estacion_evaluada.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        nota: { //nota de la estacion
            type: 'number'
        },
        porcentajeTotal: { //porcentaje total de la estacion(sumatoria porcentajes obtenidos de los elementos)
            type: 'number'
        },
        estado_aprobacion: { //aprobacion de la estacion
            type: "string"
        },

        participanteEv: { //vinculo que lo contecta con la estacion propietaria
            model: 'Participantes',
            required: true

        },
        items: { // atributo "virtual" para asociar diferentes estaciones evaluadas(uno a mucho)
            collection: 'item_evaluado', // tipo de modelo a colleccionar
            via: 'Evaluacion_ID' // nombre atributo el cual tendra relacion en modelo de estacion.js   ej= this.id=estacion.ECOEid
        },

        estacionId: { //vinculo que lo contecta con la estacion propietaria
            model: 'Estacion',
            required: true
        },
        observacion: {
            type: 'string'
        }




    },

};