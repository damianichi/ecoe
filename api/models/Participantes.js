/**
 * Participantes.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {

        estudianteId: {
            model: 'estudiante'
        },
        visualizacion: { //visualizacion resultados de la participacion
            type: "boolean",
            defaultsTo: false
                //     required:true
        },
        notaEcoe: {
            type: "number"
        },
        Estado_aprobacion_Ecoe: {
            type: "string"
        },

        ECOEid: {
            model: 'ecoe'
        },
        evaluacion: {
            collection: 'Estacion_evaluada',
            via: 'participanteEv'
        }



    },

};