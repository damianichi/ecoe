/**
 * Contenido.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
    tableName: 'Contenido_ecoe',
    attributes: {
        texto: {
            type: 'string',
            columnType: 'longtext'
                //nombre de la estacion
        },
        /* observacion: { //observacion que deja el coordinador al revisar
             type: 'string'

         },*/
        image: {
            type: 'string',
            columnType: 'longtext'
        },
        image2: {
            type: 'string',
            columnType: 'longtext'
        },
        image3: {
            type: 'string',
            columnType: 'longtext'
        },

        ElementosEv: {
            collection: 'ElementoEvaluacion',
            via: 'idContenido'
        },
        estacionProp: { //vinculo que lo contecta con la estacion propietaria
            model: 'Estacion',
            unique: true
        }


    },

};