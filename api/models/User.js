/**
 * User.js
 *
 * A user who can log in to this application.
 */

module.exports = {

    attributes: {

        emailAddress: {
            type: 'string',
            required: true,
            unique: true,
            isEmail: true,
            maxLength: 200,
            example: 'mary.sue@example.com'
        },

        //emailStatus: {
        //   type: 'string',
        //  isIn: ['unconfirmed', 'change-requested', 'confirmed'],
        //   defaultsTo: 'confirmed',
        //   description: 'The confirmation status of the user\'s email address.',
        //   extendedDescription: `Users might be created as "unconfirmed" (e.g. normal signup) or as "confirmed" (e.g. hard-coded
        //admin users).  When the email verification feature is enabled, new users created via the
        //signup form have \`emailStatus: 'unconfirmed'\` until they click the link in the confirmation email.
        //Similarly, when an existing user changes their email address, they switch to the "change-requested"
        //email status until they click the link in the confirmation email.`
        //      },


        password: {
            type: 'string',
            required: true,
            description: 'Securely hashed representation of the user\'s login password.',
            protect: true,
            example: '2$28a8eabna301089103-13948134nad'
        },

        fullName: {
            type: 'string',
            required: true,
            description: 'Full representation of the user\'s name.',
            maxLength: 120,
            example: 'Mary Sue van der McHenst'
        },
        tipoUsuario: {
            type: 'string',
            description: 'puede ser estudiante, docente, coordinador, o admin',
            defaultsTo: "alumno",
        },
        isSuperAdmin: {
            type: 'boolean',
            description: 'Whether this user is a "super admin" with extra permissions, etc.',
            extendedDescription: `Super administrador.`
        },



    },


};