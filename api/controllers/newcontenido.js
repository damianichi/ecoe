module.exports = {


    friendlyName: 'Newcontenido',


    description: 'Newcontenido something.',

    inputs: {

        image: {
            type: 'string',
            description: 'A (very) brief description of the item.'
        },
        image2: {
            type: 'string',
            description: 'A (very) brief description of the item.'
        },
        image3: {
            type: 'string',
            description: 'A (very) brief description of the item.'
        },

        texto: {
            type: 'string',
            description: 'A (very) brief description of the item.'
        },
        estacionProp: {
            type: 'number'
        }

    },


    exits: {

        success: {
            outputDescription: 'Contenido ingresado correctamente`.',
            outputExample: {}
        },
    },


    fn: async function(inputs) {
        //sails.log.debug("inputs---------------------------")
        //sails.log.debug(inputs)
        var newOb = {
            texto: inputs.texto,
            estacionProp: inputs.estacionProp,
        }
        if (inputs.image) {
            Object.assign(newOb, { image: inputs.image })
        }

        if (inputs.image2) {
            Object.assign(newOb, { image2: inputs.image2 })
        }

        if (inputs.image3) {
            Object.assign(newOb, { image3: inputs.image3 })
        }
        // Upload the image.
        sails.log.debug("otro------------")
       // sails.log.debug(newOb)
        var newContenido = await Contenido.create(newOb).fetch();
        if (newContenido) { //actualiza estado
            await Estacion.updateOne({ id: newContenido.estacionProp }).set({
                estado_preparacion: 'espera-revision'
            });
        }

        sails.log("new content subido");

        return;

    }


};