module.exports = {


    friendlyName: 'Registro ecoe',


    description: '',


    inputs: {
        nombreEcoe: {
            type: 'string'
        },
        nEstaciones: {
            type: 'number',
            required: true
        },
        minutos: {
            type: 'number',
            required: true
        },
        entretiempo: {
            type: 'number',
            required: true
        },
        estaciones_para_aprobar: {
            type: 'number',
            required: true
        },

        fecha: {
            type: 'ref',
            columnType: "datetime",
            required: true,
        },
        estadoEcoe: {
            type: 'string',
            defaultsTo: 'no-preparado' //inicialmente siempre se debe registrar como no preparado
        },

    },
    exits: {
        success: {
            description: 'Nuevo ECOE CREADO.',
            //viewTemplatePath: 'pages/preparacion/ecoe/ecoe'

            //redirige
        },

        invalid: {
            responseType: 'badRequest',
            description: 'datos no validos.',

        },
        redirect: {
            responseType: 'redirect',
            description: 'Requesting user is logged in, so redirect to the internal welcome page.'
        },
    },


    fn: async function(inputs) {
        sails.log(this.req.session.userId);

        let cordinador = await Coordinador.findOne({ //busca el id-cordinador del usuario
            where: { idUser: this.req.session.userId },
            select: ['id']
        });

        let ecoe1 = await Ecoe.create({ //perciste dato
                nombreEcoe: inputs.nombreEcoe,
                nEstaciones: inputs.nEstaciones,
                fecha: inputs.fecha,
                estadoEcoe: inputs.estadoEcoe,
                coordinadorEcoe: cordinador.id, //ingresa id obtenido anteriormente
                minutos: inputs.minutos,
                entretiempo: inputs.entretiempo,
                estaciones_para_aprobar: inputs.estaciones_para_aprobar

            })
            .fetch();
        for (var i = 1; i < inputs.nEstaciones + 1; i++) {
            await Estacion.create({
                nombre: "Estación Nº" + i,
                estado_ejecucion: "no-preparado",
                estado_preparacion: "no-revisado",
                docenteid: null,
                ECOEid: ecoe1.id
            })
            sails.log.debug("creando estacion" + i);
        }
        sails.log(ecoe1); //muestra lo ingresado
        let ecoe2 = await Ecoe.findOne({ id: ecoe1.id }).populate('estaciones');
        sails.log.debug(ecoe2);

        if (ecoe1) {
            var url = '/estaciones/' + ecoe1.id + '?';
            return url;
        };

        return {

        };



    }


};
3