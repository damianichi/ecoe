module.exports = {


    friendlyName: 'View new ecoe',


    description: 'Display "New ecoe" page.',


    exits: {

        success: {
            statusCode: 200,
            viewTemplatePath: 'pages/preparacion/ecoe/new-ecoe'
        }

    },


    fn: async function() {

        // Respond with view.
        return {};

    }


};