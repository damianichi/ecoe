module.exports = {


    friendlyName: 'View ecoe 2',


    description: 'controlador para pagina de /ecoes.',


    exits: {

        success: {
            viewTemplatePath: 'pages/preparacion/ecoe/ecoe'
        },
        redirect: {
            responseType: 'redirect',
            description: 'Requesting user is logged in, so redirect to the internal welcome page.'
        },

    },


    fn: async function() {
        if (this.req.me.tipoUsuario != "coordinador") { // si no es coordinador, no puede entrar a esta vista(registro de profe)
            throw { redirect: '/' };
        }
        var idcord = await Coordinador.findOne({ idUser: this.req.session.userId });
        this.req.session.idusuario = idcord.id; //guarda en session el id  de tipo /coordinador || docente || alumno
        sails.log.debug("el id del cordinador es : " + idcord.id);
        var ecoes = await Ecoe.find({ //busca el id-cordinador del usuario
            where: { coordinadorEcoe: idcord.id }
        }).sort([
            { createdAt: 'DESC' },
        ]);
        sails.log.debug(ecoes);
        return {
            ecoes

        };

    }


};