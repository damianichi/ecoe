module.exports = {


  friendlyName: 'Modificacionparticipantes',


  description: 'Modificacionparticipantes something.',


  inputs: {

    ingreso:{
      type:[{
        rut: 'number',
        nombre: 'string',
        id: 'number'
        }]

    },

    eliminacion:{
      type:[{
        rut: 'number',
        nombre: 'string',
        id: 'number'
      }]
    },
    idECOE:{
      type:'number'
    }

  },


  exits: {

  },


  fn: async function (inputs) {
    sails.log.debug("MOSTRANDO ARREGLOS MODIFICAION")
    sails.log.debug(inputs.ingreso);
    sails.log.debug(inputs.idECOE);

    let ecoe = await Ecoe.findOne({id: inputs.idECOE});
    if(ecoe){//verificacion si el ecoe existe
      sails.log.debug("existe ecoe")
      if(inputs.ingreso.length>0){ //existen elementos que ingresar??
        sails.log.debug("existe ingreso")

        for(let i=0;i<inputs.ingreso.length; i++){
          if(await Estudiante.findOne({id:inputs.ingreso[i].id})){ //verificamos si el estudiante existe
            sails.log.debug("existe estudiante");

            if( !(await Participantes.findOne({estudianteId:inputs.ingreso[i].id, ECOEid: inputs.idECOE}))){ //verificamos si existe ya en participantes, si no existe.. ingresamos nuevo
              sails.log.debug("ingresando nuevo")

              await Participantes.create({estudianteId: inputs.ingreso[i].id,  //se ingresa nuevo participante
                                      ECOEid:inputs.idECOE});
            }
          }
        }

        
      }
      let participante;
      if(inputs.eliminacion.length>0){ // existen elementos que eliminar?
        sails.log.debug("existen elementos para eliminar");
        for(let j=0;j<inputs.eliminacion.length; j++){ 
          sails.log.debug("--------buscando--------");

          sails.log.debug(inputs.eliminacion[j].id);
          sails.log.debug(inputs.idECOE)
          

          participante=await Participantes.findOne({estudianteId:inputs.eliminacion[j].id, ECOEid: inputs.idECOE})
          sails.log.debug("repitiendo"+j)
          sails.log.debug(participante);

          if(participante){ //verifica si existe participante

            sails.log.debug("estoy eliminando "+ inputs.eliminacion[j].id +"---"+ inputs.idECOE)
            await Participantes.destroyOne({estudianteId:inputs.eliminacion[j].id, ECOEid: inputs.idECOE}); //elimina el participante
          }
        }

      }


    }


    return;

  }


};
