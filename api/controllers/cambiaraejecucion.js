module.exports = {


    friendlyName: 'Cambiar a preparacion',


    description: '',


    inputs: {
        idECOE: {
            type: 'number'
        }


    },


    exits: {

    },


    fn: async function(inputs) {
        sails.log.debug(inputs.idECOE);
        sails.log.debug("entre al action de cambiar estado");
        var ecoe = await Ecoe.findOne({ id: inputs.idECOE });
        sails.log.debug(ecoe.estadoEcoe);
        if (ecoe && ecoe.estadoEcoe == "completado") {
            await Ecoe.updateOne({ id: inputs.idECOE }).set({ estadoEcoe: 'en-ejecucion' });
        }




        return;

    }



};