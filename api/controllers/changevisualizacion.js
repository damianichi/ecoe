module.exports = {


    friendlyName: 'Change visualizacion',


    description: '',


    inputs: {
        idParticipante: {
            type: "number"
        }
    },


    exits: {

    },


    fn: async function(inputs) {
        sails.log.debug(inputs)
        let participante = await Participantes.findOne({ id: inputs.idParticipante })
        sails.log.debug("el participante es-------")


        if (participante) {
            sails.log.debug(participante)
            let ParticipanteActualizado = await Participantes.updateOne({ id: inputs.idParticipante })
                .set({
                    visualizacion: !participante.visualizacion
                });
            sails.log.debug("y actualizado -----");
            sails.log.debug(ParticipanteActualizado);
            return ParticipanteActualizado;
        } else {
            return { statusCode: 400 };
        }

    }


};