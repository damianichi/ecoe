module.exports = {


  friendlyName: 'Modificarelemento',


  description: 'Modificarelemento something.',


  inputs: {
        item: {
            type: 'string'

        },
        nota: {
            type: 'string'
        },
        porcentajeMedio: {
            type: 'number',
            required: true
        },

        porcentajeLogrado: {
            type: 'number',
            required: true
        },
        idEditar: {
            type: 'number'
        }

  },


  exits: {

  },


  fn: async function (inputs) {
    //validar buscando la existencia del elemento.
    if(inputs.idEditar){
      await ElementoEvaluacion.updateOne({id: inputs.idEditar}).set({item:inputs.item, nota: inputs.nota, porcentajeLogrado: inputs.porcentajeLogrado, porcentajeMedio: inputs.porcentajeMedio});
    }
    return;

  }


};
