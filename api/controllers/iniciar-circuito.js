module.exports = {


    friendlyName: 'Iniciar circuito',


    description: '',


    inputs: {
        idECOE: {
            type: 'string'
        }

    },


    exits: {

    },
    fn: async function(inputs) {
        sails.log.debug("entre al iniciar y mandare un broadcast");
        // FALTA VALIDAR AQUI QUE LAS ESTACIONES ESTAN TODAS PREPARADAS
        var ecoe = await Ecoe.findOne({ id: inputs.idECOE });
        sails.log.debug(ecoe)
        var timeNow = new Date().getTime(); //slow
        if (ecoe.tiempoInicioPausa != 0) { //si tengo un tiempo de pausa iniciado, lo cierro y acumulo el tiempo en la columna de tiempo acumulado
            let tiempoPausa = timeNow - ecoe.tiempoInicioPausa; // se calcula el tiempo de pausa a partir de la date pausada y la date actual
            let tiempoAcumuladoPausa = ecoe.tiempoAcumulado + tiempoPausa; // el tiempo pausado se suma al tiempo acumulado de pausa
            var update = await Ecoe.updateOne({ id: inputs.idECOE }).set({ estadoEcoe: 'en-ejecucionEvaluacion', tiempoInicioPausa: 0, tiempoAcumulado: tiempoAcumuladoPausa }); //se resetea tiempoInicio de pausa y se sustituye la acumulacion de tiempo en tiempoAcumulado de pausa
            if (update) {
                sails.sockets.broadcast(inputs.idECOE, 'estado-de-inicio', 'inició el ecoe');
                sails.log.debug("ya mande el broadcast");
            }
            return { idEcoe: inputs.idECOE };

        } else {
            var update = await Ecoe.updateOne({ id: inputs.idECOE }).set({ estadoEcoe: 'en-ejecucionEvaluacion', tiempoInicio: timeNow });
            if (update) {
                sails.sockets.broadcast(inputs.idECOE, 'estado-de-inicio', 'inició el ecoe');
                sails.log.debug("ya mande el broadcast");
            }
            return { idEcoe: inputs.idECOE };
        }
    }
};