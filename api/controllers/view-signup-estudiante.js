module.exports = {


  friendlyName: 'View signup estudiante',


  description: 'Display "Signup estudiante" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/signup-estudiante'



    },

    redirect: {
      responseType: 'redirect',
      description: 'Requesting user is logged in, so redirect to the internal welcome page.'
  },

  },


  fn: async function () {
            if (this.req.me.tipoUsuario != "coordinador") { // si no es coordinador, no puede entrar a esta vista(registro de profe)
              if(this.req.me.isSuperAdmin!=1){      
                throw { redirect: '/' };
              }
        }
    // Respond with view.
    return {};

  }


};
