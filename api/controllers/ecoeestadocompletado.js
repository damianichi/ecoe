module.exports = {


    friendlyName: 'Ecoeestadocompletado',


    description: 'Ecoeestadocompletado something.',


    inputs: {
        idECOE: {
            type: 'number'
        }


    },


    exits: {

    },


    fn: async function(inputs) {
        sails.log.debug(inputs.idECOE);
        sails.log.debug("entre al action de cambiar estado");
        var ecoe = await Ecoe.findOne({ id: inputs.idECOE }).populate('participantesECOE');
        sails.log.debug(ecoe.estadoEcoe);
        if (ecoe && ecoe.estadoEcoe != "completado") {
            if (ecoe.estadoEcoe === "en-ejecucion") { //si viene desde un estado de ejecucion, restablece el estado de preparacion de los profesores para que vuelvan a confirmar 
                await Estacion.update({ ECOEid: inputs.idECOE }).set({ estado_ejecucion: 'no-preparado' });
            }
            if (ecoe.estadoEcoe === "en-ejecucionEvaluacion") { //BORRAR, solo CASO DE PRUEBA!!!!!!!!!! B
                await Estacion.update({ ECOEid: inputs.idECOE }).set({ estado_ejecucion: 'no-preparado' });
            }




            await Ecoe.updateOne({ id: inputs.idECOE }).set({ estadoEcoe: 'completado', tiempoInicioPausa: 0, tiempoAcumulado: 0, tiempoInicio: 0 });

            let participantesActualizados = await Participantes.update({ ECOEid: ecoe.id }).set({ visualizacion: 0, notaEcoe: 0, Estado_aprobacion_Ecoe: '' }).fetch();
            sails.log.debug("participantes actualizados: ", participantesActualizados)
            if (participantesActualizados) {
                for (let participante of participantesActualizados) {
                    await Estacion_evaluada.destroy({ participanteEv: participante.id })

                }
            }
            //poner todos los participantes con visualizacion 0, nota 0, estado " ",
            // iterar todos los alumnos y poner id de participacion -1 en participanteEv
        }




        return;

    }


};