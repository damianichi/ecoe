module.exports = {


    friendlyName: 'Finalizarexamen',


    description: 'Finalizarexamen something.',


    inputs: {
        idEcoe: {
            type: 'number'
        }

    },


    exits: {
        redirect: {
            responseType: 'redirect',
            description: 'Requesting user is logged in, so redirect to the internal welcome page.'
        },
    },


    fn: async function(inputs) {


        if (this.req.me && this.req.me.tipoUsuario == "coordinador") {
            sails.log.debug(this.req.me.id) //id del coordinador
            var coordinador = await Coordinador.findOne({ idUser: this.req.me.id })
            var ecoe = await Ecoe.findOne({ id: inputs.idEcoe }).populate('participantesECOE').populate('estaciones');

            if (coordinador && ecoe) {
                if (ecoe.estadoEcoe === "en-ejecucionEvaluacion") {
                    for (var i = 0; i < ecoe.participantesECOE.length; i++) {
                        var promedio = 0;
                        var suma = 0;
                        var EstadoAprobacion = '';
                        var cantEstacionesAprobadas = 0;
                        var evaluacion = await Estacion_evaluada.find({ where: { participanteEv: ecoe.participantesECOE[i].id } });
                        Object.assign(ecoe.participantesECOE[i], {
                            evaluacion: evaluacion
                        });
                        if (evaluacion) {

                            for (var j = 0; j < evaluacion.length; j++) {
                                suma += Number(evaluacion[j].nota)
                                if (evaluacion[j].estado_aprobacion === 'aprobado') {
                                    cantEstacionesAprobadas++;
                                }
                            }
                            promedio = (suma / Number(ecoe.estaciones.length));
                        }
                        if (cantEstacionesAprobadas >= ecoe.estaciones_para_aprobar) {
                            estadoAprobacion = 'aprobado';
                        } else {
                            estadoAprobacion = 'reprobado';
                        }
                        var evaluacionFinal = await Participantes.update({ id: ecoe.participantesECOE[i].id }).set({ notaEcoe: promedio, Estado_aprobacion_Ecoe: estadoAprobacion }).fetch();
                    }
                    await Ecoe.update({ id: ecoe.id }).set({ estadoEcoe: 'ejecutado' });
                    return;
                }


            }
            sails.log.debug(ecoe);
            throw { redirect: '/' }; //askdlmlaksdmlasd si estoy logeado y soy coordinador .. pa aca
        } else {
            throw { redirect: '/' }; //askdlmlaksdmlasd si estoy logeado y soy coordinador .. pa aca
        }



        return;

    }


};