module.exports = {


    friendlyName: 'Registroelemento',


    description: 'Registroelemento something.',


    inputs: {
        item: {
            type: 'string'

        },
        nota: {
            type: 'string'
        },
        porcentajeMedio: {
            type: 'number',
            required: true
        },

        porcentajeLogrado: {
            type: 'number',
            required: true
        },
        idContenido: {
            type: 'number'
        }


    },


    exits: {
        success: {
            description: 'elemento de rubrica registrado.',

        },


    },


    fn: async function(inputs) {

        var elemento = await ElementoEvaluacion.create({
            item: inputs.item,
            nota: inputs.nota,
            porcentajeMedio: inputs.porcentajeMedio,
            porcentajeLogrado: inputs.porcentajeLogrado,
            idContenido: inputs.idContenido
        });

        sails.log.debug("elemento ingresado===> " + elemento);
        return;

    }


};