module.exports = {


  friendlyName: 'Ingresarobservacion',


  description: 'Ingresarobservacion something.',


  inputs: {
    observacion:{
      type: 'string'
    },
    idEstacion:{
      type:'number'
    },


  },


  exits: {

  },


  fn: async function (inputs) {
    
    //revisar que estacion aun existe..... PENDIENTE 

    var estacion= await Estacion.updateOne({id: inputs.idEstacion}).set({observacion: inputs.observacion, estado_preparacion: 'en-revision'}); //ingresa la observacion y cambia de estacion a "en-revision"

    // All done.
    return;

  }


};
