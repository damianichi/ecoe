module.exports = {


    friendlyName: 'Getstatus',


    description: 'Getstatus something.',


    inputs: {
        idECOE: {
            type: 'string'
        }

    },


    exits: {

    },


    fn: async function(inputs) {
        //sails.log.debug(inputs.idECOE);
        var ECOE = await Ecoe.findOne({ id: inputs.idECOE }).populate('estaciones')
        if (ECOE.estaciones) {

            for (var i = 0; i < ECOE.estaciones.length; i++) { //recorre las estaciones
                let objectoIntegrar = await Docente.findOne({ where: { id: ECOE.estaciones[i].docenteid }, select: ['nombre'] }); //obtiene nombre del ecoe dependiente el id registrado en la estacion
                // sails.log.debug(objectoIntegrar);
                Object.assign(ECOE.estaciones[i], { //se agrega al objeto a retornar
                    nombreProfesor: objectoIntegrar.nombre,

                });
            }

            return ECOE.estaciones;
        }

        return;

    }


};