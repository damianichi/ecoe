module.exports = {


    friendlyName: 'View preevaluacion',


    description: 'Display "Preevaluacion" page.',



    inputs: {
        idEstacion: {
            type: 'string'
        }
    },

    exits: {

        success: {
            viewTemplatePath: 'pages/preevaluacion'
        },
        redirect: {
            responseType: 'redirect',
            description: 'ya confirmo, se redirige a la evaluacion'
        },


    },


    fn: async function({ idEstacion }) {
        var url = require('url');
        var util = require('util');
        var estacion = await Estacion.findOne({ id: idEstacion }).populate('contenido');

        var ecoe = await Ecoe.findOne({ id: estacion.ECOEid });
        //sails.log.debug("IMPRIMIENDO ESTACIONS");
        if (ecoe.estadoEcoe != "en-ejecucion") {
            if (ecoe.estadoEcoe === "en-ejecucionEvaluacion" || ecoe.estadoEcoe === "en-pausa") {
                throw { redirect: '/ejecucionecoe/' + idEstacion }; //direccion para ir a evaluar alumnos
            } else {
                throw { redirect: '/estacion/contenido/' + idEstacion }; // si esta en preparacion o terminado
            }
        }
        let dataECOE = { ecoe: JSON.parse(JSON.stringify(ecoe)), estacion: JSON.parse(JSON.stringify(estacion)) };
        //sails.log.debug(dataECOE);
        //sails.log.debug(dataECOE.estacion.contenido);
        if (dataECOE.estacion.estado_ejecucion == "preparado") {
            //sails.log.debug(dataECOE.estacion.estado_preparacion + "  -----estaaaaadooooooooooooooooo")
            throw { redirect: '/ejecucionecoe/' + dataECOE.estacion.id }; //askdlmlaksdmlasd si estoy logeado y soy un docente xD me tira acá
        }
        return {
            dataECOE: dataECOE,
        };
    }
};