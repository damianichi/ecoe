module.exports = {


  friendlyName: 'Modificarcontenidoprofesor',


  description: 'Modificarcontenidoprofesor something.',


  inputs: {
    id: {
      type:'number'
    },

    image: {
        type: 'string',
        description: 'A (very) brief description of the item.'
    },
    image2: {
        type: 'string',
        description: 'A (very) brief description of the item.'
    },
    image3: {
        type: 'string',
        description: 'A (very) brief description of the item.'
    },

    texto: {
        type: 'string',
        description: 'A (very) brief description of the item.'
    },

  },
  success: {
    outputDescription: 'Contenido ingresado correctamente`.',
    outputExample: {}
  },


  fn: async function (inputs) {
    sails.log.debug("CAI EN LA MODIFICACION")
    
    var newOb = {
        texto: inputs.texto,
    }
    if (inputs.image) {
        Object.assign(newOb, { image: inputs.image })
    }

    if (inputs.image2) {
        Object.assign(newOb, { image2: inputs.image2 })
    }else{
      Object.assign(newOb, { image2: '' })
     }

    if (inputs.image3) {
        Object.assign(newOb, { image3: inputs.image3 })
    }else{
      Object.assign(newOb, { image3: '' })
     }

    //sails.log.debug("reque:" , newOb)
    var updatedcontent = await Contenido.updateOne({ id: inputs.id })
    .set(newOb);
  
    if (updatedcontent) {
      sails.log('Contenido actualizado.');
    }
    else {
      sails.log('Error no se pudo actualizar el contenido.');
}
    return;


  }


};
