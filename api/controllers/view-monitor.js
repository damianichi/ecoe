module.exports = {


    friendlyName: 'View monitor',


    description: 'Display "Monitor" page.',
    inputs: {
        idEcoe: {
            type: 'number'
        }
    },


    exits: {

        success: {
            viewTemplatePath: 'pages/monitor'
        },


        redirect: {
            responseType: 'redirect',
            description: '.'
        },

    },


    fn: async function({ idEcoe }) {
        //sails.log.debug("ID EcOE")
        //sails.log.debug(idEcoe)


        var ecoe = await Ecoe.findOne({ id: idEcoe }).populate('participantesECOE').populate('estaciones');
        //sails.log.debug("ECOE")
        //sails.log.debug(ecoe)
        //sails.log.debug("----------------------------------------")
        if (ecoe.estadoEcoe != 'en-ejecucionEvaluacion') { //si el ecoe esta en ejecucion o en modo evaluacion
            if (ecoe.estadoEcoe != 'en-pausa') {
                if (ecoe.estadoEcoe === 'en-ejecucion') {
                    //sails.log.debug("cai en el segundo  if con: ")
                    throw { redirect: '/estaciones/' + idEcoe };
                } else {
                    throw { redirect: '/ecoes' };

                }
            }

        } else { //si esta en cualquier estado que no sea en ejecucion o en modo evaluacion
            if (this.req.me && this.req.me.tipoUsuario == "coordinador") {

                //throw { redirect: '/estaciones/' + idEcoe };

            }
        }



        if (ecoe) {

            for (var i = 0; i < ecoe.participantesECOE.length; i++) { //obteniene nombre y rut para cada id de participante y lo agrega al objeto a retornar
                var evaluacion = await Estacion_evaluada.find({ where: { participanteEv: ecoe.participantesECOE[i].id } });

                //sails.log.debug("evaluacion")
                //sails.log.debug(evaluacion[0])




                if (evaluacion) {
                    if (evaluacion.length > 0) {
                        for (var j = 0; j < evaluacion.length; j++) {
                            var estacion = await Estacion.findOne({ where: { id: evaluacion[j].estacionId } });
                            Object.assign(evaluacion[j], {

                                nombreEstacion: estacion.nombre
                            });
                        }
                    }
                    Object.assign(ecoe.participantesECOE[i], {

                        evaluacion: evaluacion
                    });
                }

                var objectoIntegrar = await Estudiante.findOne({ where: { id: ecoe.participantesECOE[i].estudianteId }, select: ['nombre', 'rut'] });
                //sails.log.debug(objectoIntegrar);
                Object.assign(ecoe.participantesECOE[i], { //se agrega al objeto a retornar
                    nombreParticipante: objectoIntegrar.nombre,
                    rut: objectoIntegrar.rut
                });
            }

        }

        //sails.log.debug(ecoe)


        var horaactual = new Date().getTime();
        // Respond with view.
        return { ecoe: ecoe, horaactual: horaactual };

    }


};