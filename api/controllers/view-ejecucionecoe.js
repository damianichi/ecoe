module.exports = {


    friendlyName: 'View ejecucionecoe',


    description: 'Display "Ejecucionecoe" page.',
    inputs: {
        idEstacion: {
            type: 'number'
        }
    },

    exits: {

        success: {
            viewTemplatePath: 'pages/ejecucionecoe'
        },
        redirect: {
            responseType: 'redirect',
            description: '.'
        },

    },


    fn: async function({ idEstacion }) {


        var esperandoInicio = true;
        estacion = await Estacion.findOne({ id: idEstacion });


        sails.log.debug("----------------------__ESTACION-------------------------")
        sails.log.debug("id:" + idEstacion)
        sails.log.debug("-----------------------------------------------")
        sails.log.debug(estacion);
        ecoe = await Ecoe.findOne({ id: estacion.ECOEid }).populate('participantesECOE').populate('estaciones');
        sails.log.debug("----------------------end-------------------------")




        if (ecoe.estadoEcoe === 'en-ejecucion' || ecoe.estadoEcoe === 'en-ejecucionEvaluacion' || ecoe.estadoEcoe === 'en-pausa') { //si el ecoe esta en ejecucion o en modo evaluacion
            if (estacion.estado_ejecucion != 'preparado') { //en caso de que la estacion aun no este preparada la redirigira a la vista para el boton de preparacion
                sails.log.debug("cai en el segundo  if con: " + estacion.estado_ejecucion)
                throw { redirect: '/' + idEstacion + '/preevaluacion?' };
            }
        } else { //si esta en cualquier estado que no sea en ejecucion o en modo evaluacion
            sails.log.debug("cai en el primer if con: " + ecoe.estadoEcoe)
            throw { redirect: '/docente' };
        }
        /*
                if (ecoe.estadoEcoe != 'en-ejecucion' ){
                    sails.log.debug("cai en el primer if con: " + ecoe.estadoEcoe)
                    throw { redirect: '/docente' };
                } else {
                    if (estacion.estado_ejecucion != 'preparado') {
                        sails.log.debug("cai en el segundo  if con: " + estacion.estado_ejecucion)
                        throw { redirect: '/' + idEstacion + '/preevaluacion?' };
                    }
                }
        */
        //sails.log.debug(ecoe);

        if (ecoe) {

            for (var i = 0; i < ecoe.participantesECOE.length; i++) { //obteniene nombre y rut para cada id de participante y lo agrega al objeto a retornar
                // sails.log.debug("EN EL FOR");
                var evaluacion = await Estacion_evaluada.findOne({ where: { estacionId: idEstacion, participanteEv: ecoe.participantesECOE[i].id } });
                //sails.log.debug("idEstacion")
                // sails.log.debug(idEstacion)
                if (evaluacion) {
                    Object.assign(ecoe.participantesECOE[i], {

                        evaluacion: evaluacion
                    });
                }
                //sails.log.debug(ecoe.participantesECOE[i].estudianteId);
                var objectoIntegrar = await Estudiante.findOne({ where: { id: ecoe.participantesECOE[i].estudianteId }, select: ['nombre', 'rut'] });
                // sails.log.debug(objectoIntegrar);
                Object.assign(ecoe.participantesECOE[i], { //se agrega al objeto a retornar
                    nombreParticipante: objectoIntegrar.nombre,
                    rut: objectoIntegrar.rut
                })
            }
            if (ecoe.estadoEcoe === 'en-ejecucionEvaluacion') {
                esperandoInicio = false;
            }
        }

        var horaactual = new Date().getTime();
        // sails.log.debug(horaactual)

        //sails.log.debug("-------------------------------------------MEDIO");
        //sails.log.debug(ecoe);

        return { estacion: estacion, ecoe: ecoe, esperandoInicio: esperandoInicio, horaactual: horaactual };

    }


};