module.exports = {


    friendlyName: 'Borrarelemento',


    description: 'Borrarelemento something.',


    inputs: {
        idBorrar: { type: 'number' },

    },


    exits: {

    },


    fn: async function(inputs) {
        sails.log.debug("ENTRANDO AL ACTION DE BORRAR elemento");
        //sails.log.debug(inputs.);
        var eliminado = await ElementoEvaluacion.destroyOne({ id: inputs.idBorrar });
        // All done.
        return;

    }


};