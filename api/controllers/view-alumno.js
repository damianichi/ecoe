module.exports = {


    friendlyName: 'View alumno',


    description: 'Display "Alumno" page.',


    exits: {

        success: {
            viewTemplatePath: 'pages/alumno'
        },
        redirect: {
            responseType: 'redirect',
            description: 'Requesting user is logged in, so redirect to the internal welcome page.'
        },

    },


    fn: async function() {
        if (this.req.me.tipoUsuario != "estudiante") {

            throw { redirect: '/' };
        }
        let estudiante = await Estudiante.findOne({ idUser: this.req.me.id });
        let elemetosElimnar = [];
        if (estudiante) {
            let ecoes = await Participantes.find({ estudianteId: estudiante.id });
            for (var i = 0; i < ecoes.length; i++) {
                if (ecoes[i].ECOEid === null) {
                    elemetosElimnar.push(i);
                } else {
                    let objectoIntegrar = await Ecoe.findOne({ where: { id: ecoes[i].ECOEid }, select: ['nombreEcoe'] });
                    //sails.log.debug(objectoIntegrar);
                    Object.assign(ecoes[i], { //se agrega al objeto a retornar
                        nombreEcoe: objectoIntegrar.nombreEcoe

                    });
                }
            }
            if (elemetosElimnar.length > 0) {
                for (var j = elemetosElimnar.length - 1; j >= 0; j--) {
                    ecoes.splice(elemetosElimnar[j], 1);
                }
            }
            return { ecoes };
        } else {
            throw { redirect: '/' };
        }





    }


};