module.exports = {


    friendlyName: 'View contenido',


    description: 'Display "Contenido" page.',

    inputs: {
        idEstacion: {
            type: 'string'
        }
    },


    exits: {

        success: {
            viewTemplatePath: 'pages/contenido'
        },
        redirect: {
            responseType: 'redirect',
            description: 'Requesting user is logged in, so redirect to the internal welcome page.'
        },

    },


    fn: async function({ idEstacion }) {
        var url = require('url');
        var util = require('util');
        var estacion = await Estacion.findOne({ id: idEstacion }).populate('contenido');

        var ecoe = await Ecoe.find({ where: { id: estacion.ECOEid }, select: ['nombreEcoe'] });
        //sails.log.debug("el ecooooooeee")
        //sails.log.debug(ecoe)
        if (ecoe.length === 0) {
            //sails.log.debug("NO EXISTE")
            throw { redirect: '/' }; //askdlmlaksdmlasd si estoy logeado y soy coordinador .. pa aca
        }
        //sails.log.debug("IMPRIMIENDO ESTACIONS");
        let dataECOE = { ecoe: JSON.parse(JSON.stringify(ecoe))[0], estacion: JSON.parse(JSON.stringify(estacion)) };

        //sails.log.debug(dataECOE);
        //sails.log.debug(dataECOE.estacion.contenido);
        return {
            dataECOE: dataECOE,
        };

    }


};