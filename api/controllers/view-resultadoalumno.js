module.exports = {


    friendlyName: 'View resultadoalumno',


    description: 'Display "Resultadoalumno" page.',

    inputs: {
        idParticipacion: {
            type: 'string'
        }
    },

    exits: {

        success: {
            viewTemplatePath: 'pages/resultadoalumno'
        },
        redirect: {
            responseType: 'redirect',
            description: 'Requesting user is logged in, so redirect to the interna.'
        },


    },


    fn: async function({ idParticipacion }) {
        if (this.req.me.tipoUsuario != "estudiante") { //redirige si no son estudiants
            sails.log.debug("no es estudiante")
            throw { redirect: '/' };
        }
        participacion = await Participantes.findOne({
            where: { id: idParticipacion },
            select: ['id', 'ECOEid', 'Estado_aprobacion_Ecoe', 'notaEcoe', 'estudianteId']
        }).populate('evaluacion', {
            select: ['estacionId', 'nota', 'nota', 'porcentajeTotal', 'estado_aprobacion']
        });
        if (!participacion) { //si no existe la participacion redirige al home de estudiante

            throw { redirect: '/' };
        } else {
            estudiante = await Estudiante.findOne({ id: participacion.estudianteId })
            if (estudiante.idUser != this.req.me.id) { //redirige si no es "propietario" de la participacion

                throw { redirect: '/' };
            }
        }
        let cont = 0
        for (var i = 0; i < participacion.evaluacion.length; i++) { //obteniene nombre y rut para cada id de participante y lo agrega al objeto a retornar
            if (participacion.evaluacion[i].estado_aprobacion === "aprobado") {
                cont++;
            }
            var objectoIntegrar = await Estacion.findOne({ where: { id: participacion.evaluacion[i].estacionId }, select: ['nombre'] });
            Object.assign(participacion.evaluacion[i], { //se agrega al objeto a retornar
                nombreEstacion: objectoIntegrar.nombre,
            });
        }
        Object.assign(participacion, { //se agrega al objeto a retornar
            n_estaciones_aprobadas: cont,
        });

        //sails.log.debug(participacion);
        return { participacion };

    }


};