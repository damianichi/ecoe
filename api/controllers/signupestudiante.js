module.exports = {


  friendlyName: 'Signupestudiante',


  description: 'Signupestudiante something.',


  inputs: {


    emailAddress: {
      required: true,
      type: 'string',
      isEmail: true,
  },
  rut:{
    type:'number'
  },

  password: {
      required: true,
      type: 'string',
      maxLength: 200,
  },

  fullName: {
      required: true,
      type: 'string',
  },
  tipoUsuario: {
      type: 'string',
  },

  },


  exits: {
    success: {
      description: 'nuevo alumno ingresado.'

  },
  invalid: {
      responseType: 'badRequest',
      description: 'Tfalta algun dato.',

  },
  emailAlreadyInUse: {
      statusCode: 409,
      description: 'email usado.',
  },


  },


  fn: async function (inputs) {
    sails.log.debug(inputs.tipoUsuario);
    sails.log.debug(inputs);

    var newEmailAddress = inputs.emailAddress.toLowerCase();

    var newUserRecord = await User.create(_.extend({
            emailAddress: newEmailAddress,
            password: await sails.helpers.passwords.hashPassword(inputs.password),
            fullName: inputs.fullName,
            //tosAcceptedByIp: this.req.ip,
            tipoUsuario: inputs.tipoUsuario
        }, sails.config.custom.verifyEmailAddresses ? {
            emailProofToken: await sails.helpers.strings.random('url-friendly'),
            emailProofTokenExpiresAt: Date.now() + sails.config.custom.emailProofTokenTTL,
            emailStatus: 'unconfirmed'
        } : {}))
        .intercept('E_UNIQUE', 'emailAlreadyInUse')
        .intercept({ name: 'UsageError' }, 'invalid')
        .fetch();

    if (inputs.tipoUsuario == "estudiante" && newUserRecord) { //si es coordinador, lo registra ademas en tabla de coordinadores //separado de la tabla de datos
        await Estudiante.create({
            idUser: newUserRecord.id,
            rut: inputs.rut,
            nombre: inputs.fullName,
            correo: newEmailAddress
        });
    }


    if (sails.config.custom.enableBillingFeatures) {
        let stripeCustomerId = await sails.helpers.stripe.saveBillingInfo.with({
            emailAddress: newEmailAddress
        }).timeout(5000).retry();
        await User.updateOne({ id: newUserRecord.id })
            .set({
                stripeCustomerId
            });
    }

    // All done.
    return;

  }


};
