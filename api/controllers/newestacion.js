module.exports = {


    friendlyName: 'Newestacion',


    description: 'Newestacion something.',


    inputs: {
        idECOE: {
            type: 'number'
        },
        NombreEstación: {
            type: 'string'
        },
        estaciones_para_aprobar: {
            type: 'number'
        }


    },


    exits: {

    },


    fn: async function(inputs) {


        await Ecoe.updateOne({ id: inputs.idECOE }).set({ estadoEcoe: "no-preparado", estaciones_para_aprobar: inputs.estaciones_para_aprobar }).then(
            await Estacion.create({
                nombre: "Estación " + inputs.NombreEstación,
                estado_ejecucion: "no-preparado",
                estado_preparacion: "no-revisado",
                docenteid: null,
                ECOEid: inputs.idECOE
            })
        )


        return;

    }


};