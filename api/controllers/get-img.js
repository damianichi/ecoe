module.exports = {


    friendlyName: 'Get img',


    description: '',


    inputs: {

        id: {
            description: 'id del contenido donde esta guardada la informacion de la imagen -',
            type: 'number',
            required: true
        }

    },


    exits: {

        success: {
            outputDescription: 'perfect',
            outputType: 'ref'
        },

        forbidden: { responseType: 'forbidden' },

        notFound: { responseType: 'notFound' },
        redirect: {
            responseType: 'redirect',
            description: 'Requesting user is logged in, so redirect to the internal welcome page.'
        },

    },


    fn: async function({ id }) {
        if (this.req.me && !(this.req.me.tipoUsuario == "coordinador" || this.req.me.tipoUsuario == "docente")) {
            sails.log("(redireccion)tipousuarioeninicio " + this.req.me.tipoUsuario);
            throw { redirect: '/' }; //askdlmlaksdmlasd redirige cuando el usuario no es coordinador o docente
        }
        var contenido = await Contenido.findOne({ id });
        if (!contenido) { throw 'notFound'; }

        this.res.type(contenido.imageUploadMime);
        var downloading = await sails.startDownload(contenido.imageUploadFd);



        // All done.
        return downloading;

    }


};