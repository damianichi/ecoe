module.exports = {


    friendlyName: 'Vincularprofesorestacion',


    description: 'Vincularprofesorestacion something.',


    inputs: {
        idDocente: {
            type: 'number'
        },
        idEstacionVincular: {
            type: 'number'
        }


    },


    exits: {

    },


    fn: async function(inputs) {
        sails.log.debug("iddocente: " + inputs.idDocente);

        sails.log.debug("id estacion: " +
            inputs.idEstacionVincular);
        var estacion = await Estacion.updateOne({ id: inputs.idEstacionVincular })
            .set({
                docenteid: inputs.idDocente
            });



        return inputs.idEstacionVincular;

    }


};