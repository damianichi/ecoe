module.exports = {


    friendlyName: 'Pausarecoe',


    description: 'Pausarecoe something.',



    inputs: {
        idECOE: {
            type: 'number'
        },
        tipo: { //0=pausa, 1= reanudar,
            type: 'boolean'
        }


    },


    exits: {

    },


    fn: async function(inputs) {
        sails.log.debug(inputs.idECOE);
        sails.log.debug("entre al action de pausar");

        var ecoe = await Ecoe.findOne({ id: inputs.idECOE });
        sails.log.debug(ecoe.estadoEcoe);
        var now1 = new Date().getTime(); //slow
        if (ecoe && ecoe.estadoEcoe == "en-ejecucionEvaluacion") {
            await Ecoe.updateOne({ id: inputs.idECOE }).set({ estadoEcoe: 'en-ejecucion', tiempoInicioPausa: now1 });
            sails.log.debug("PAUSADO!!!----------")
            return;
        } else {
            return;
        }
    }


};