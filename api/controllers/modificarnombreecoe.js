module.exports = {


    friendlyName: 'Modificarnombreecoe',


    description: 'Modificarnombreecoe something.',


    inputs: {
        id: {
            type: 'number'
        },
        nombre: {
            type: 'string'
        },
        fecha: {
            type: 'ref',
            columnType: "datetime",
        },
        minutos: {
            type: 'number'
        },
        entretiempo: {
            type: 'number'
        },
        estaciones_para_aprobar: {
            type: 'number'
        },

    },


    exits: {

    },


    fn: async function(inputs) {

        if (inputs.nombre != '') {
            await Ecoe.updateOne({ id: inputs.id }).set({
                nombreEcoe: inputs.nombre,
                fecha: inputs.fecha,
                minutos: inputs.minutos,
                entretiempo: inputs.entretiempo,
                estaciones_para_aprobar: inputs.estaciones_para_aprobar
            });
        }

        // All done.
        return;

    }


};