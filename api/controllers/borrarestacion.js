module.exports = {


    friendlyName: 'Borrarestacion',


    description: 'Borrarestacion something.',

    inputs: {
        idBorrar: { type: 'number' },
        idECOE: {
            type: 'number'
        },
        estaciones_para_aprobar: {
            type: 'number'
        }
    },


    exits: {

    },


    fn: async function(inputs) {

        sails.log.debug(inputs)
        await Ecoe.updateOne({ id: inputs.idECOE }).set({ estadoEcoe: "no-preparado", estaciones_para_aprobar: inputs.estaciones_para_aprobar })
        var idEcoe = await Estacion.findOne({ id: inputs.idBorrar });
        var borrado = await Estacion.destroyOne({ id: inputs.idBorrar });
        sails.log.debug("SE BORRO Estación" + borrado);
        //sails.log.debug(borrado);
        if (borrado) {
            sails.log.debug("--------------");
            sails.log.debug(idEcoe);
            var url = '/estaciones/' + idEcoe.ECOEid;
            return url;
        };
        return "/";

    }


};