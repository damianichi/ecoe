/**
 * SocketconexionController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {


    onConnect: function(req, res) {
        sails.log.debug("el id de la estacion es : " + req.body.idEcoe);
        sails.sockets.join(req, req.body.idEcoe);
        return res.ok();
    },

    sendMessage: function(req, res) {
        content = req.body.dato;
        sails.log.debug(content);
        sails.sockets.broadcast('sala-2', 'chat', content)
        return res.ok();
    },

    iniciarECOE: function(req, res) {
        sails.log.debug("entre al iniciar y mandare un broadcast");



        //var update = await Ecoe.updateOne({ id: req.body.idECOE }).set({ estadoEcoe: 'en-ejecucionEvaluacion' });

        sails.sockets.broadcast(req.body.idECOE, 'estado-de-inicio', 'inició el ecoe');
        sails.log.debug("ya mande el broadcast");
        //ingresar hora de inicio
        //nuevo estado

    }


};