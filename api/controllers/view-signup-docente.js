module.exports = {


    friendlyName: 'View signup docente',


    description: 'Display "Signup docente" page.',


    exits: {

        success: {
            viewTemplatePath: 'pages/signup-docente'
        },
        redirect: {
            responseType: 'redirect',
            description: 'Requesting user is logged in, so redirect to the internal welcome page.'
        },

    },


    fn: async function() {

        if (this.req.me.tipoUsuario != "coordinador") { // si no es coordinador, no puede entrar a esta vista(registro de profe)
            if(this.req.me.isSuperAdmin!=1){
               throw { redirect: '/' };
            }
        }

        // SI NO ES COORDINADOR :: REDIRECCIONAR
        // Respond with view.
        return {};

    }


};