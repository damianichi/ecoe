module.exports = {


    friendlyName: 'Aprobarestacion',


    description: 'Aprobarestacion something.',


    inputs: {
        idEstacion: {
            type: 'number'
        }

    },


    exits: {

    },


    fn: async function(inputs) {
        var contenido = await Contenido.findOne({ estacionProp: inputs.idEstacion }).populate('ElementosEv');
        let porcentajeMax = 0;
        for (var i = 0; i < contenido.ElementosEv.length; i++) {
            porcentajeMax += contenido.ElementosEv[i].porcentajeLogrado;

        }

        if (porcentajeMax === 100) { //validar que existe el contenido en un 100% antes de cambiar estado
            await Estacion.updateOne({ id: inputs.idEstacion }).set({
                estado_preparacion: 'revisado', observacion: ''
            });
        }
        return;

    }


};