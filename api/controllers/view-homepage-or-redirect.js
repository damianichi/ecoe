module.exports = {


    friendlyName: 'View homepage or redirect',


    description: 'Display or redirect to the appropriate homepage, depending on login status.',


    exits: {

        success: {
            statusCode: 200,
            description: 'Requesting user is a guest, so show the public landing page.',
            viewTemplatePath: 'pages/entrance/login'
        },

        redirect: {
            responseType: 'redirect',
            description: 'Requesting user is logged in, so redirect to the internal welcome page.'
        },

    },


    fn: async function() {


        if (this.req.me && this.req.me.tipoUsuario == "coordinador") {
            sails.log("(redireccion)tipousuarioeninicio " + this.req.me.tipoUsuario);
            throw { redirect: '/ecoes' }; //askdlmlaksdmlasd si estoy logeado y soy coordinador .. pa aca
        }
        if (this.req.me && this.req.me.tipoUsuario == "docente") {
            sails.log("(redireccion)tipousuarioeninicio " + this.req.me.tipoUsuario);
            throw { redirect: '/docente' }; //askdlmlaksdmlasd si estoy logeado y soy coordinador .. pa aca
        }
        if (this.req.me && this.req.me.tipoUsuario == "estudiante") {
            sails.log("(redireccion)tipousuarioeninicio " + this.req.me.tipoUsuario);
            throw { redirect: '/alumno' }; //askdlmlaksdmlasd si estoy logeado y soy coordinador .. pa aca
        }

        if (this.req.me && this.req.me.tipoUsuario == "admin") {
            throw { redirect: '/welcome' }; //askdlmlaksdmlasd si estoy logeado y soy un docente xD me tira acá
        }

        return {};

    }


};