module.exports = {


    friendlyName: 'View evaluacion',


    description: 'pagina donde se evaluada un alumno',
    inputs: {
        idestacion: { type: 'number' },
        idparticipante: { type: 'number' }

    },


    exits: {

        success: {
            viewTemplatePath: 'pages/evaluacion'
        },
        redirect: {
            responseType: 'redirect',
            description: 'Sin permisos'
        },

    },


    fn: async function(inputs) {
        if (this.req.me.tipoUsuario != "docente") {

            throw { redirect: '/' };
        }

        // redirigir si el alumno ya tiene evaluacion, 

        // redirigir si el ecoe no esta en estado de evaluacion.

        //redirigir si el participante ya tiene una evaluacion.



        var estacion = await Estacion.findOne({ id: inputs.idestacion }).populate('contenido');
        if (!estacion) {
            throw { redirect: '/' }
        }
        var contenido = await Contenido.findOne({ id: estacion.contenido[0].id }).populate('ElementosEv');
        if (estacion.ECOEid) {
            var ecoe = await Ecoe.findOne({ where: { id: estacion.ECOEid }, select: ['nombreEcoe', 'estadoEcoe', 'tiempoInicio', 'minutos', 'entretiempo', 'tiempoAcumulado'] }).populate('estaciones').populate('participantesECOE');
        }

        if (ecoe.estadoEcoe != "en-ejecucionEvaluacion") {
            //sails.log.debug("ESTADO DEL ECOE" + ecoe.estadoEcoe + (ecoe.estadoEcoe != "en-ejecucionEvaluacion"))
            if (ecoe.estadoEcoe === "en-ejecucion") {
                if (estacion.estado_ejecucion != 'preparado') { //en caso de que la estacion aun no este preparada la redirigira a la vista para el boton de preparacion
                    throw { redirect: '/' + inputs.idestacion + '/preevaluacion?' };
                }
                throw { redirect: '/' + inputs.idestacion + '/preevaluacion?' };

            }
            throw { redirect: '/ejecucionecoe/' + inputs.idestacion }; //re dirige si el ecoe no esta en ejecucion
        }
        var alumno = await Participantes.findOne({ id: inputs.idparticipante })
        let objectoIntegrar = await Estudiante.findOne({ where: { id: alumno.estudianteId }, select: ['nombre', 'rut'] });
        Object.assign(alumno, { //se agrega al objeto a retornar
            nombreParticipante: objectoIntegrar.nombre,
            rut: objectoIntegrar.rut
        });

        let horaactual = new Date().getTime();
        //sails.log.debug(horaactual)


        return {
            ecoe: ecoe,
            estacion: estacion,
            elementos: contenido.ElementosEv,
            alumno: alumno,
            horaactual: horaactual,
        };

    }


};