module.exports = {


    friendlyName: 'Enviarevaluacion',


    description: 'Enviarevaluacion something.',


    inputs: {
        porcentajes: {
            type: [{
                id: 'number',
                item: 'string',
                porcentajeObtenido: 'number'
            }]

        },
        observacion: {
            type: 'string'
        },
        idEstacion: {
            type: 'number'
        },
        alumnoID: {
            type: 'number'
        }
    },


    exits: {
        badRequest: {
            description: 'El alumno ya tiene una evaluacion registrada para esta estación',
            responseType: 'badRequest',
            statusCode: 400,

        },

        outTime: {
            description: 'ingreso fuera del tiempo de evaluacion',
            responseType: 'badRequest',
            statusCode: 408,


        }

    },


    fn: async function(inputs) {
        var sumatoriaPorcentaje = 0;
        var aprobacion = 'reprobado';

        let evaluacion = await Estacion_evaluada.findOne({
            where: {
                participanteEv: inputs.alumnoID,
                estacionId: inputs.idEstacion
            }

        });


        // ------------validaciones Ingreso--------
        // PENDIENTE
        if (this.req.me && this.req.me.tipoUsuario == "docente") {
            var estacion = await Estacion.findOne({ id: inputs.idEstacion });
            var ecoe = await Ecoe.findOne({ id: estacion.ECOEid }).populate('estaciones');
            if (ecoe.estadoEcoe === "en-ejecucionEvaluacion") {
                var tiempo = new Date().getTime() //get time de ahora
                var timeInicial = ecoe.timeInicial;
                var N_estaciones = ecoe.estaciones.length;
                var tiempoLimite = timeInicial + ecoe.minutos * N_estaciones + ecoe.entretiempo * N_estaciones + ecoe.entretiempo;
                //revisar tiempos con respecto a la pausa y cantindad de alumnos!!
                if (tiempo > tiempoLimite) {
                    if (!evaluacion) {
                        await Estacion_evaluada.create({
                            nota: 1.0,
                            porcentajeTotal: 0,
                            estado_aprobacion: aprobacion,
                            participanteEv: inputs.alumnoID,
                            estacionId: inputs.idEstacion,
                            observacion: "ingreso fuera del tiempo correspondiente."
                        })
                    } else {
                        return { statusCode: 400 }
                        // throw 'badRequest'
                    }
                } else {
                    if (tiempo > timeInicial) {
                        return { statusCode: 408 }
                    }
                }


            }
        }
        //1. debe ser docente
        //2. debe estar en estado de ejecucion.
        //3. debe estar dentro del tiempo de ejecucion.
        //---------------- FIN VALIDACION INCIAL------

        for (var i = 0; i < inputs.porcentajes.length; i++) {
            sumatoriaPorcentaje += Number(inputs.porcentajes[i].porcentajeObtenido);
        }
        //------------CALCULO DE NOTA CON 60% exigencia
        if (sumatoriaPorcentaje >= 60) {
            aprobacion = 'aprobado';
        }
        var puntaje = sumatoriaPorcentaje;
        if (puntaje < 60) {
            puntaje = 3 * puntaje / 60 + 1;
        } else {
            puntaje = 3 * (puntaje - 60) / 40 + 4;
        }
        var Nota = Math.round(Math.trunc(puntaje * 100) / 100 * 10) / 10;
        //--------------Fin calculo nota.


        //let evaluacion = await Estacion_evaluada.findOne({ estacionId: inputs.idEstacion });
        /* sails.log.debug("sumatoriaPorcentaje: " + sumatoriaPorcentaje);
         sails.log.debug("aprobacion: " + aprobacion);
         sails.log.debug("Nota: " + Nota)
         sails.log.debug("idEstacion: " + inputs.idEstacion)
         sails.log.debug("idParticipante: " + inputs.alumnoID)
         sails.log.debug("observacion: " + inputs.observacion)*/ //print de pruebas

        if (!evaluacion) {
            var evaluada = await Estacion_evaluada.create({
                nota: Nota,
                porcentajeTotal: sumatoriaPorcentaje,
                estado_aprobacion: aprobacion,
                participanteEv: inputs.alumnoID,
                estacionId: inputs.idEstacion,
                observacion: inputs.observacion

            }).fetch()
            sails.log.debug("estacion evaluada")
            sails.log.debug(evaluada)

        } else {
            return { statusCode: 400 }

        }






        //sails.log.debug(inputs)
        return;

    }


};