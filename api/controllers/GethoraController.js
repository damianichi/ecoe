/**
 * GethoraController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    obtenerHora: function(req, res, next) {
        console.log("estoy en controller de hora");

        var f = new Date().getTime();


        return res.send({
            'horaactual': f
        })
    },

};