module.exports = {


    friendlyName: 'View welcome page',


    description: 'Display the dashboard "Welcome" page.',


    exits: {

        success: {
            viewTemplatePath: 'pages/dashboard/welcome',
            description: 'Display the welcome page for authenticated users.'
        },
        redirect: {
            responseType: 'redirect',
            description: 'Requesting user is logged in, so redirect to the internal welcome page.'
        },

    },


    fn: async function() {
        if (this.req.me.tipoUsuario != "admin") {

            throw { redirect: '/' };
        }
        var a = 'vengo de dentro';
        return {};

    }


};