module.exports = {


    friendlyName: 'Confirmacionprofesor',


    description: 'Confirmacionprofesor something.',


    inputs: {
        idestacion: {
            type: "number"
        }

    },


    exits: {

    },


    fn: async function(inputs) {
        sails.log.debug(inputs);
        var estacion = await Estacion.findOne({ id: inputs.idestacion });

        if (estacion) {
            ecoe = await Ecoe.findOne({ id: estacion.ECOEid });

            if (ecoe.estadoEcoe === "en-ejecucion" && estacion.estado_ejecucion === "no-preparado") {
                await Estacion.updateOne({ id: inputs.idestacion }).set({
                    estado_ejecucion: 'preparado'
                });
            } else {
                return {
                    msg: "redirect"
                }
            }
        } else {
            return {
                msg: "redirect"
            }
        }




        return;

    }


};