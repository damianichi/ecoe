module.exports = {


    friendlyName: 'Getdocentes',


    description: 'Getdocentes something.',


    inputs: {

    },


    exits: {
        redirect: {
            responseType: 'redirect',
            description: 'Requesting user is logged in, so redirect to the internal welcome page.'
        },


    },


    fn: async function(inputs) {
        if (this.req.me.tipoUsuario != "coordinador") { // si no es coordinador, no puede recibir los profesores xD
            throw { redirect: '/' };
        }

        var docentes = await Docente.find({ select: ['id', 'nombre'] });
        return docentes;

    }


};