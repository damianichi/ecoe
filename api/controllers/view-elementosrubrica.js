module.exports = {


    friendlyName: 'View elementosrubrica',


    description: 'Display "Elementosrubrica" page.',

    inputs: {
        idEstacion: { type: 'number' },

    },

    exits: {

        success: {
            viewTemplatePath: 'pages/elementosrubrica'
        },
        redirect: {
            responseType: 'redirect',
            description: 'Requesting user is logged in, so redirect to the internal welcome page.'
        },

    },


    fn: async function({ idEstacion }) {


        var estacion = await Estacion.findOne({ id: idEstacion }).populate('contenido');
        if (estacion.contenido.length == 0) { //si el contenido no existe.. redirige a la pag para ingresar contenido a la estacion
            throw { redirect: '/estacion/contenido/' + estacion.id };
        } else {
            var contenido = await Contenido.find({ id: estacion.contenido[0].id }).populate('ElementosEv');
        }
        if (estacion.ECOEid) {
            var ecoe = await Ecoe.find({ where: { id: estacion.ECOEid }, select: ['nombreEcoe', 'estadoEcoe'] });
        }
        //sails.log.debug(contenido);
        return {
            ecoe: ecoe[0],
            estacion: estacion,
            elementos: contenido[0].ElementosEv

        };

    }


};