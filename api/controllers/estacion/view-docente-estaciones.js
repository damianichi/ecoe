module.exports = {


    friendlyName: 'View docente estaciones',


    description: 'Display "Docente estaciones" page.',


    exits: {

        success: {
            viewTemplatePath: 'pages/preparacion/estacion/docenteEstaciones'
        },
        redirect: {
            responseType: 'redirect',
            description: 'Requesting user is logged in, so redirect to the internal welcome page.'
        },

    },


    fn: async function() {
        if (this.req.me.tipoUsuario != "docente") { // si no es coordinador, no puede entrar a esta vista(registro de profe)
            throw { redirect: '/' };
        }

        var iddoc = await Docente.findOne({ idUser: this.req.session.userId });
        //sails.log.debug("mostrando id docente: " +iddoc);
        this.req.session.idusuario = iddoc.id; //guarda en session el id  de tipo /coordinador || docente || alumno

        var estaciones = await Estacion.find({ //busca el id-cordinador del usuario
            where: { docenteid: iddoc.id }
        }).populate('contenido').sort([
            { createdAt: 'DESC' },
        ]);

        let elemetosElimnar = [];
        if (estaciones) {
            for (var i = 0; i < estaciones.length; i++) { //recorre las estaciones

                if (estaciones[i].ECOEid === null) {
                    elemetosElimnar.push(i);

                } else {
                    let objectoIntegrar = await Ecoe.findOne({ where: { id: estaciones[i].ECOEid }, select: ['nombreEcoe', 'estadoEcoe'] }); //obtiene nombre del ecoe dependiente el id registrado en la estacion
                    if (objectoIntegrar) {
                        //sails.log.debug(objectoIntegrar);
                        Object.assign(estaciones[i], { //se agrega al objeto a retornar
                            nombreEcoe: objectoIntegrar.nombreEcoe,
                            estadoEcoe: objectoIntegrar.estadoEcoe
                        });

                    }
                }




            }
        }
        if (elemetosElimnar.length > 0) {
            //sails.log.debug("entre1")
            for (var j = elemetosElimnar.length - 1; j >= 0; j--) {
                //sails.log.debug("entre2")
                //sails.log.debug(elemetosElimnar[j])
                estaciones.splice(elemetosElimnar[j], 1);
            }
        }

        return {
            estaciones

        };



    }


};