module.exports = {


    friendlyName: 'View estacion',


    description: 'Display "Estacion" page.',

    inputs: {
        id: {
            type: 'string'
        }
    },


    exits: {

        success: {
            viewTemplatePath: 'pages/preparacion/estacion/estaciones'
        },

        redirect: {
            responseType: 'redirect',
            description: 'Requesting user is logged in, so redirect to the interna.'
        },

    },


    fn: async function({ id }) {
        //sails.log.debug(this.req.session);
        var iniciarEcoe = true;
        var EstadoPreparacion = false;
        var ECOE = await Ecoe.findOne({ id: id }).populate('estaciones').populate('participantesECOE');
        //var ECOEParticipantes = await Ecoe.findOne({ id: id }).populate('participantesECOE');
        //var participantes= ECOEParticipantes.participantesECOE

        if (ECOE) {

            if (ECOE.estadoEcoe === 'en-ejecucionEvaluacion' || ECOE.estadoEcoe === 'en-pausa') {
                throw { redirect: '/monitor/' + ECOE.id };
            }
            if (ECOE.estadoEcoe === 'ejecutado') {
                throw { redirect: '/resultados/' + ECOE.id };
            }
            for (var i = 0; i < ECOE.participantesECOE.length; i++) { //obteniene nombre y rut para cada id de participante y lo agrega al objeto a retornar
                let objectoIntegrar = await Estudiante.findOne({ where: { id: ECOE.participantesECOE[i].estudianteId }, select: ['nombre', 'rut'] });
                //sails.log.debug(objectoIntegrar);
                Object.assign(ECOE.participantesECOE[i], { //se agrega al objeto a retornar
                    nombreParticipante: objectoIntegrar.nombre,
                    rut: objectoIntegrar.rut
                });
            }
            if (ECOE.estaciones) {
                // sails.log.debug("entre")
                for (var i = 0; i < ECOE.estaciones.length; i++) { //recorre las estaciones
                    if (ECOE.estaciones[i].docenteid) {
                        let objectoIntegrar = await Docente.findOne({ where: { id: ECOE.estaciones[i].docenteid }, select: ['nombre'] }); //obtiene nombre del ecoe dependiente el id registrado en la estacion
                        // sails.log.debug(objectoIntegrar);
                        Object.assign(ECOE.estaciones[i], { //se agrega al objeto a retornar
                            nombreProfesor: objectoIntegrar.nombre,

                        });

                        if (ECOE.estaciones[i].estado_ejecucion != "preparado") {
                            this.iniciarEcoe = false;

                        }


                    } else {
                        this.iniciarEcoe = false;

                    }

                }
            }
            if (ECOE.estadoEcoe === "en-ejecucion") {
                EstadoPreparacion = true;
            }
            if (ECOE.estadoEcoe === 'en-ejecucionEvaluacion') {
                //hacer un redirect para pagina de monitorizacion coordinador... xD
            }
        }
        //sails.log.debug(ECOE);
        //sails.log.debug(ECOEParticipantes);

        var estudiantes = await Estudiante.find();

        // Respond with view.


        return { ECOE, estudiantes, EstadoPreparacion, iniciarEcoe };

    }


};