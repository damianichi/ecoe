module.exports = {


    friendlyName: 'View resultados',


    description: 'Display "Resultados" page.',

    inputs: {
        idEcoe: {
            type: 'string'
        }
    },
    exits: {

        success: {
            viewTemplatePath: 'pages/resultados'
        },
        redirect: {
            responseType: 'redirect',
            description: 'NO está en estado  ejecutado.'
        },

    },


    fn: async function(inputs) {
        var ecoe = await Ecoe.findOne({ id: inputs.idEcoe }).populate('estaciones').populate('participantesECOE');
        if (ecoe.estadoEcoe != 'ejecutado') {
            throw { redirect: '/estaciones/' + ecoe.id };
        }
        /*if (ecoe) {

            for (var i = 0; i < ecoe.participantesECOE.length; i++) { //obteniene nombre y rut para cada id de participante y lo agrega al objeto a retornar

                var objectoIntegrar = await Estudiante.findOne({ where: { id: ecoe.participantesECOE[i].estudianteId }, select: ['nombre', 'rut'] });
                sails.log.debug(objectoIntegrar);
                Object.assign(ecoe.participantesECOE[i], { //se agrega al objeto a retornar
                    nombreParticipante: objectoIntegrar.nombre,
                    rut: objectoIntegrar.rut
                });
            }

        }*/
        if (ecoe) {

            for (var i = 0; i < ecoe.participantesECOE.length; i++) { //obteniene nombre y rut para cada id de participante y lo agrega al objeto a retornar
                var evaluacion = await Estacion_evaluada.find({ where: { participanteEv: ecoe.participantesECOE[i].id } });

                //sails.log.debug("evaluacion")
                //sails.log.debug(evaluacion[0])
                if (evaluacion) {
                    let cont = 0;
                    if (evaluacion.length > 0) {

                        for (var j = 0; j < evaluacion.length; j++) {
                            if (evaluacion[j].estado_aprobacion === "aprobado") {
                                cont++;
                            }
                            var estacion = await Estacion.findOne({ where: { id: evaluacion[j].estacionId } });
                            Object.assign(evaluacion[j], {

                                nombreEstacion: estacion.nombre
                            });
                        }


                    }
                    Object.assign(ecoe.participantesECOE[i], {
                        n_estaciones_aprobadas: cont,
                        evaluacion: evaluacion
                    });
                }
                var objectoIntegrar = await Estudiante.findOne({ where: { id: ecoe.participantesECOE[i].estudianteId }, select: ['nombre', 'rut'] });
                //sails.log.debug(objectoIntegrar);
                Object.assign(ecoe.participantesECOE[i], { //se agrega al objeto a retornar
                    nombreParticipante: objectoIntegrar.nombre,
                    rut: objectoIntegrar.rut
                });
            }

        }

        // Respond with view.
        return { ecoe: ecoe };

    }


};